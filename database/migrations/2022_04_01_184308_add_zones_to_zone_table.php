<?php

use App\Models\Zone;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddZonesToZoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('zona')->insert([
            [
                'id' => 1,
                'nombre' => 'Contacto',
                'descripcion' => 'Vista de contacto en aplicacion',
                'ancho' => '250',
                'alto' => '80',
                'costo' => '100',
                'activo' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 2,
                'nombre' => 'Pagina Inicial',
                'descripcion' => 'Vista de inicio de aplicacion',
                'ancho' => '250',
                'alto' => '80',
                'costo' => '200',
                'activo' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 3,
                'nombre' => 'Avisos',
                'descripcion' => 'Vista de avisos en la aplicacion',
                'ancho' => '250',
                'alto' => '80',
                'costo' => '100',
                'activo' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 4,
                'nombre' => 'Tarifas',
                'descripcion' => 'Vista de tarifas en la aplicacion',
                'ancho' => '250',
                'alto' => '80',
                'costo' => '100',
                'activo' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
