<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValuesToZone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zona', function (Blueprint $table) {
            $table->string('descripcion');
			$table->integer('ancho');
			$table->integer('alto');
			$table->integer('costo');
			$table->boolean('activo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zona', function (Blueprint $table) {
            //
        });
    }
}
