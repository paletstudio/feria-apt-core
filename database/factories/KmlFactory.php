<?php

namespace Database\Factories;

use App\Models\Kml;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class KmlFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Kml::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'route_name' => $this->faker->name,
            'description' => Str::slug($this->faker->text),
            'url' => 'kmls/lUsdml5vzvfDvubJPm027mhuiZU03BG4i98oqrlQ.xml',
            'active' => $this->faker->boolean,
        ];
    }
}
