<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

use App\Models\Tenant\Notification;

class NotificationEvent
{
    use SerializesModels, Dispatchable;

    public $title;
    public $message;
    public $subtitle;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($title, $message, $subtitle = null)
    {
        $this->title = $title;
        $this->message = $message;
        $this->subtitle = $subtitle;
    }
}
