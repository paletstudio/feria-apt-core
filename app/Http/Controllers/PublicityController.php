<?php

namespace App\Http\Controllers;

use App\Models\Publicity;
use App\Models\Zone;
use App\Presenters\PublicityPresenter;
use App\Presenters\ZonePresenter;
use App\QueryBuilder\Filters\PublicityDescriptionFilter;
use App\QueryBuilder\Filters\PublicityEndDateFilter;
use App\QueryBuilder\Filters\PublicityInitDateFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Inertia\Inertia;

class PublicityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = QueryBuilder::for(Publicity::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::custom('descripcion', new PublicityDescriptionFilter),
                AllowedFilter::custom('fecha_inicio', new PublicityInitDateFilter),
                AllowedFilter::custom('fecha_fin', new PublicityEndDateFilter),
                AllowedFilter::exact('activo'),
            ])
            ->paginate($request->input('per_page', 10));

        return Inertia::render('Publicity/Index', [
            'data' => PublicityPresenter::collection($data)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $zones = Zone::where('activo', true)->get();

        return Inertia::render('Publicity/Create', [
            'zones' => ZonePresenter::collection($zones)->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'descripcion' => 'string|required',
            'zona_id' => 'numeric|required',
            'fecha_inicio' => 'string|required',
            'fecha_fin' => 'string|required',
            'activo' => 'boolean|required',
            'link' => 'string|required',
            'banner' => 'required'
        ]);

        $publicity = new Publicity();
        $publicity->descripcion = $validated['descripcion'];
        $publicity->zona_id = $validated['zona_id'];
        $publicity->fecha_inicio = $validated['fecha_inicio'];
        $publicity->fecha_fin = $validated['fecha_fin'];
        $publicity->link = $validated['link'];
        $publicity->activo = $validated['activo'];

        if ($request->hasFile('banner')) {
            $path = "public/publicidad";
            $filename = $request->banner->store($path);
            $publicity->banner = str_replace('public/', '', $filename);
        }

        $publicity->save();

        return Redirect::route('publicity.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Publicity $publicity)
    {
        $publicity->load(['zone']);

        return Inertia::render('Publicity/Show', [
            'publicity' => PublicityPresenter::make($publicity)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Publicity $publicity)
    {
        $zones = Zone::where('activo', true)->get();

        $publicity->load(['zone']);

        return Inertia::render('Publicity/Edit', [
            'publicity' => PublicityPresenter::make($publicity)->get(),
            'zones' => ZonePresenter::collection($zones)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publicity $publicity)
    {
        $validated = $request->validate([
            'descripcion' => 'string|required',
            'zona_id' => 'numeric|required',
            'fecha_inicio' => 'string|required',
            'fecha_fin' => 'string|required',
            'activo' => 'boolean|required',
            'link' => 'string|required',
            'banner' => 'required'
        ]);

        $publicity->descripcion = $validated['descripcion'];
        $publicity->zona_id = $validated['zona_id'];
        $publicity->fecha_inicio = $validated['fecha_inicio'];
        $publicity->fecha_fin = $validated['fecha_fin'];
        $publicity->link = $validated['link'];
        $publicity->activo = $validated['activo'];

        if ($request->hasFile('banner')) {
            $path = "public/publicidad";
            $filename = $request->banner->store($path);
            $publicity->banner = str_replace('public/', '', $filename);
        }

        $publicity->update();

        return Redirect::route('publicity.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
