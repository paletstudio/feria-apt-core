<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Models\Version;
use App\Presenters\VersionPresenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Notifications/Index', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendNotification(Request $request)
    {
        $validated = $request->validate([
            'title' => 'string|required|max:20',
            'message' => 'string|required|max:60',
        ]);

        event(new NotificationEvent($validated['title'], $validated['message']));

        return Redirect::route('notifications.index');
    }
}
