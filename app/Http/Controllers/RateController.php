<?php

namespace App\Http\Controllers;

use App\Models\Tarifas;
use App\Presenters\RatePresenter;
use App\QueryBuilder\Filters\RateAcFilter;
use App\QueryBuilder\Filters\RateActiveFilter;
use App\QueryBuilder\Filters\RateDateInitFilter;
use App\QueryBuilder\Filters\RateEndDateFilter;
use App\QueryBuilder\Filters\RateNameFilter;
use App\QueryBuilder\Filters\RatePriceFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Inertia\Inertia;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = QueryBuilder::for(Tarifas::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::custom('nombre', new RateNameFilter),
                AllowedFilter::exact('tipo'),
                AllowedFilter::custom('ac', new RateAcFilter),
                AllowedFilter::custom('precio', new RatePriceFilter),
                AllowedFilter::custom('fecha_tarifa', new RateDateInitFilter),
                AllowedFilter::custom('fecha_fin', new RateEndDateFilter),
                AllowedFilter::custom('activo', new RateActiveFilter),
            ])
            ->paginate($request->input('per_page', 10));

        return Inertia::render('Rates/Index', [
            'data' => RatePresenter::collection($data)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return Inertia::render('Rates/Create', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nombre' => 'string|required',
            'tipo' => 'numeric|required',
            'precio' => 'numeric|required',
            'fecha_fin' => 'string|required',
            'fecha_tarifa' => 'string|required',
            'ac' => 'boolean|required',
            'activo' => 'boolean|required',
        ]);

        $rate = new Tarifas();
        $rate->nombre = $validated['nombre'];
        $rate->tipo = $validated['tipo'];
        $rate->precio = $validated['precio'];
        $rate->fecha_fin = $validated['fecha_fin'];
        $rate->fecha_tarifa = $validated['fecha_tarifa'];
        $rate->ac = $validated['ac'];
        $rate->activo = $validated['activo'];
        $rate->save();

        return Redirect::route('rates.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tarifas $rate)
    {
        return Inertia::render('Rates/Show', [
            'rate' => RatePresenter::make($rate)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarifas $rate)
    {
        return Inertia::render('Rates/Edit', [
            'rate' => RatePresenter::make($rate)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tarifas $rate)
    {
        $validated = $request->validate([
            'nombre' => 'string|required',
            'tipo' => 'numeric|required',
            'precio' => 'numeric|required',
            'fecha_fin' => 'string|required',
            'fecha_tarifa' => 'string|required',
            'ac' => 'boolean|required',
            'activo' => 'boolean|required',
        ]);

        $rate->nombre = $validated['nombre'];
        $rate->tipo = $validated['tipo'];
        $rate->precio = $validated['precio'];
        $rate->fecha_fin = $validated['fecha_fin'];
        $rate->fecha_tarifa = $validated['fecha_tarifa'];
        $rate->ac = $validated['ac'];
        $rate->activo = $validated['activo'];
        $rate->update();

        return Redirect::route('rates.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
