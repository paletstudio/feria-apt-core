<?php

namespace App\Http\Controllers;

use App\Models\Version;
use App\Presenters\VersionPresenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class VersionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $version = Version::orderBy('id', 'desc')->limit(1)->first();
        return Inertia::render('Version/Index', [
            'data' => VersionPresenter::make($version)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'version' => 'string|required',
        ]);

        $version = new Version();
        $version->version = $validated['version'];
        $version->date = Carbon::now();
        $version->save();

        return Redirect::route('version.index');
    }
}
