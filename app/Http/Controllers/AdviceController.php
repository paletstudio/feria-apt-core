<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Models\Advice;
use App\Presenters\AdvicePresenter;
use App\QueryBuilder\Filters\AdviceActiveFilter;
use App\QueryBuilder\Filters\AdviceDateFilter;
use App\QueryBuilder\Filters\AdviceSubtitleFilter;
use App\QueryBuilder\Filters\AdviceTitleFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Inertia\Inertia;

class AdviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = QueryBuilder::for(Advice::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::custom('titulo', new AdviceTitleFilter),
                AllowedFilter::custom('subtitulo', new AdviceSubtitleFilter),
                AllowedFilter::custom('fecha', new AdviceDateFilter),
                AllowedFilter::custom('activo', new AdviceActiveFilter),
            ])
            ->paginate($request->input('per_page', 10));

        return Inertia::render('Advice/Index', [
            'data' => AdvicePresenter::collection($data)->get()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return Inertia::render('Advice/Create', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'titulo' => 'string|required|',
            'subtitulo' => 'string|required',
            'fecha' => 'string|required',
            'descripcion' => 'string|required',
            'activo' => 'boolean|required',
            'imagen' => 'required'
        ]);

        $advice = new Advice();
        $advice->titulo = $validated['titulo'];
        $advice->subtitulo = $validated['subtitulo'];
        $advice->fecha = $validated['fecha'];
        $advice->descripcion = $validated['descripcion'];
        $advice->activo = $validated['activo'];

        if ($request->hasFile('imagen')) {
            $path = "public/avisos";
            $filename = $request->imagen->store($path);
            $advice->imagen = str_replace('public/', '', $filename);
        }

        $advice->save();

        event(new NotificationEvent($validated['titulo'], $validated['descripcion']));

        return Redirect::route('advices.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Advice $advice)
    {
        return Inertia::render('Advice/Show', [
            'data' => AdvicePresenter::make($advice)->get()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Advice $advice)
    {
        return Inertia::render('Advice/Edit', [
            'data' => AdvicePresenter::make($advice)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advice $advice)
    {
        $validated = $request->validate([
            'titulo' => 'string|required',
            'subtitulo' => 'string|required',
            'fecha' => 'string|required',
            'descripcion' => 'string|required',
            'activo' => 'boolean|required',
            'imagen' => 'nullable'
        ]);

        $advice->titulo = $validated['titulo'];
        $advice->subtitulo = $validated['subtitulo'];
        $advice->fecha = $validated['fecha'];
        $advice->descripcion = $validated['descripcion'];
        $advice->activo = $validated['activo'];

        if ($request->hasFile('imagen')) {
            $path = "public/avisos";
            $filename = $request->imagen->store($path);
            $advice->imagen = str_replace('public/', '', $filename);
        }

        $advice->update();

        return Redirect::route('advices.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
