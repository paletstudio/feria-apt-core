<?php

namespace App\Http\Controllers;

use App\Models\Kml;
use App\Presenters\KmlPresenter;
use App\QueryBuilder\Filters\KmlActiveFilter;
use App\QueryBuilder\Filters\KmlRouteNameFilter;
use App\QueryBuilder\Filters\KmlUpdateDateFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Inertia\Inertia;
use Illuminate\Support\Str;

class KmlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = QueryBuilder::for(Kml::class)
            ->allowedFilters([
                AllowedFilter::exact('id_kml'),
                AllowedFilter::custom('route_name', new KmlRouteNameFilter),
                AllowedFilter::custom('updated_at', new KmlUpdateDateFilter),
                AllowedFilter::custom('active', new KmlActiveFilter),
            ])
            ->paginate($request->input('per_page', 10));

        return Inertia::render('Kmls/Index', [
            'data' => KmlPresenter::collection($data)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Kmls/Create', []);
    }

    public function createMasive()
    {
        return Inertia::render('Kmls/CreateMasive', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'route_name' => 'string|required|',
            'active' => 'boolean|required',
            'file' => 'required',
            'id_kml' => 'string|required',
            'geojson' => 'required',
        ]);

        $kml = new Kml();
        $kml->route_name = $validated['route_name'];
        $kml->id_kml = $validated['id_kml'];
        $kml->active = $validated['active'];
        $kml->geojson = $validated['geojson'];

        if ($request->hasFile('file')) {
            $path = "public/kmls";
            $filename = $request->file->store($path);
            $kml->url = str_replace('public/', '', $filename);
        }

        $kml->save();

        return Redirect::route('kmls.index');
    }


    public function storeMasive(Request $request)
    {
        foreach ($request->input('filesWithKml', []) as $key => $fileWithKml) {
            if ($request->hasFile("files.{$key}")) {
                $kml = new Kml();
                $kml->route_name = $fileWithKml['name'] ?? '';
                $kml->id_kml = preg_replace("/\.[^.]+$/", "", $request->file("files.{$key}")->getClientOriginalName());
                $kml->active = true;
                $kml->geojson = $fileWithKml['geojson'] ?? '';
                $path = "public/kmls";
                $filename = $request->file("files.{$key}")->store($path);
                $kml->url = str_replace('public/', '', $filename);
                $kml->save();
            }
        }

        return Redirect::route('kmls.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kml $kml)
    {
        return Inertia::render('Kmls/Show', [
            'data' => KmlPresenter::make($kml)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kml $kml)
    {
        return Inertia::render('Kmls/Edit', [
            'data' => KmlPresenter::make($kml)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kml $kml)
    {
        $validated = $request->validate([
            'route_name' => 'string|required|',
            'id_kml' => 'string|required',
            'geojson' => 'required',
            'active' => 'boolean|required',
        ]);

        $kml->route_name = $validated['route_name'];
        $kml->id_kml = $validated['id_kml'];
        $kml->active = $validated['active'];
        $kml->geojson = json_encode($validated['geojson'], JSON_NUMERIC_CHECK);

        if ($request->hasFile('file')) {
            $path = "public/kmls";
            $filename = $request->file->store($path);
            $kml->url = str_replace('public/', '', $filename);
        }

        $kml->update();

        return Redirect::route('kmls.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
