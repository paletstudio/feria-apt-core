<?php

namespace App\Http\Controllers;

use App\Models\Zone;
use App\Presenters\ZonePresenter;
use App\QueryBuilder\Filters\ZoneDescriptionFilter;
use App\QueryBuilder\Filters\ZoneNameFilter;
use App\QueryBuilder\Filters\ZoneWidthFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Inertia\Inertia;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = QueryBuilder::for(Zone::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::custom('nombre', new ZoneNameFilter),
                AllowedFilter::custom('descripcion', new ZoneDescriptionFilter),
                AllowedFilter::exact('activo'),
            ])
            ->paginate($request->input('per_page', 10));

        return Inertia::render('Zones/Index', [
            'data' => ZonePresenter::collection($data)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Zone $zone)
    {
        return Inertia::render('Zones/Show', [
            'data' => ZonePresenter::make($zone)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Zone $zone)
    {
        return Inertia::render('Zones/Edit', [
            'data' => ZonePresenter::make($zone)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zone $zone)
    {
        $validated = $request->validate([
            'nombre' => 'string|required',
            'descripcion' => 'string|required',
            'alto' => 'numeric|required',
            'ancho' => 'numeric|required',
            'costo' => 'numeric|required',
            'activo' => 'boolean|required',
        ]);

        $zone->nombre = $validated['nombre'];
        $zone->descripcion = $validated['descripcion'];
        $zone->alto = $validated['alto'];
        $zone->ancho = $validated['ancho'];
        $zone->costo = $validated['costo'];
        $zone->activo = $validated['activo'];
        $zone->update();

        return Redirect::route('zones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
