<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Kml;
use App\Presenters\KmlPresenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class KmlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Kml::query();
        $query->where('active', true);
        $lastUpdated = $request->input('lastUpdated');

        if (!empty($lastUpdated)) {
            $query->where('updated_at', '>=', Carbon::parse($request->input('lastUpdated')));
        }

        if ($request->input('pagination') === 'false') {
            $kmls = $query->get();
        } else {
            $kmls = $query->latest()
                ->paginate(10);
        }

        return response()->json(KmlPresenter::collection($kmls)->get(), 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function canKmls(Request $request)
    {
        $query = Kml::query();
        $query->where('active', true);
        $lastUpdated = $request->input('lastUpdated');

        if (!empty($lastUpdated)) {
            $query->where('updated_at', '>=', Carbon::parse($request->input('lastUpdated')));
        }

        $kmls = $query->count();

        $response = $kmls >= 1 ? true : false;

        return response()->json($response, 200);
    }
}
