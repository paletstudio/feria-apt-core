<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Advice;
use App\Models\Kml;
use App\Models\Response;
use App\Presenters\AdvicePresenter;
use App\Presenters\KmlPresenter;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = new Response();
        $paginate = request('paginate', false);
        try {
            if ($paginate) {
                $advices = Advice::paginate(10);
            } else {
                $advices = Advice::all();
                $return = $advices;
                if (request('mobile', false)) {
                    $advices = Advice::where('activo', 1)->orderBy('fecha', 'desc')->get();
                    $groupedAdvices = $advices->groupBy('fecha')->toArray();
                    $return = [];
                    foreach ($groupedAdvices as $key => $value) {
                        $temp = array('date' => $key, 'data' => $value);
                        array_push($return, $temp);
                    }
                }
            }
            $response->data = $return;
            $response->message = 'success';
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
            $response->message = 'error';
            $response->code = 500;
        }
        return response()->json($response, 200);
    }
}
