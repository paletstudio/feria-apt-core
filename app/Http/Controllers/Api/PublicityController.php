<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Publicity;
use App\Models\Response;
use App\Models\Tarifas;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PublicityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getactive(Request $request)
    {
        $response = new Response();
        $paginate = request('paginate', false);
        $zone = request('zone', null);
        $query = Publicity::with(['zone'])->where('activo', true);

        if ($zone) {
            $query->where('zona_id', $zone);
        }
        try {
            if ($paginate) {
                $publicity = $query->paginate(10);
            } else {
                $publicity = $query->get();
            }
            $response->data = $publicity;
            $response->message = 'success';
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
            $response->message = 'error';
            $response->code = 500;
        }
        return response()->json($response, 200);
    }
}
