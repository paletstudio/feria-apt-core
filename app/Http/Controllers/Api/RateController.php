<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Response;
use App\Models\Tarifas;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activeRates(Request $request)
    {
        $response = new Response;
        try {
            $now = date("Y-m-d");
            $rate = Tarifas::where([
                ['fecha_tarifa', '<=', $now],
                ['fecha_fin', '>=', $now],
                ['activo', 1]
            ])->get();

            $response->data = $rate;
            $response->message = 'success';
        } catch (ModelNotFoundException $e) {
            $response->code = 404;
            $response->message = 'not found';
            $response->exception = $e->getMessage();
        } catch (\Exception $e) {
            $response->code = 500;
            $response->message = 'error';
            $response->exception = $e->getMessage();
        }
        return response()->json($response, 200);
    }
}
