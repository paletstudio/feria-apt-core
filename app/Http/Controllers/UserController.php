<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use App\Models\Advice;
use App\Models\User;
use App\Presenters\AdvicePresenter;
use App\Presenters\UserPresenter;
use App\QueryBuilder\Filters\AdviceActiveFilter;
use App\QueryBuilder\Filters\AdviceDateFilter;
use App\QueryBuilder\Filters\AdviceSubtitleFilter;
use App\QueryBuilder\Filters\AdviceTitleFilter;
use App\QueryBuilder\Filters\UserCreatedAtFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Inertia\Inertia;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = QueryBuilder::for(User::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                'name',
                'email',
                AllowedFilter::custom('created_at', new UserCreatedAtFilter),
            ])
            ->allowedSorts([
                'id',
            ])
            ->paginate($request->input('per_page', 10));

        return Inertia::render('User/Index', [
            'data' => UserPresenter::collection($data)->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('User/Create', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'string|required|max:255',
            'email' => 'unique:users,email',
            'password' => 'string|required|max:255'
        ]);

        if (!empty($validated['password'])) {
            $validated['password'] = Hash::make($validated['password']);
        }

        User::create($validated);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return Inertia::render('User/Show', [
            'data' => UserPresenter::make($user)->get(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return Inertia::render('User/Edit', [
            'data' => UserPresenter::make($user)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validated = $request->validate([
            'name' => 'string|required|max:255',
            'email' => 'unique:users,email,' . $user->id,
            'password' => 'string|max:255'
        ]);

        if (!empty($validated['password'])) {
            $validated['password'] = Hash::make($validated['password']);
        }

        $user->update($validated);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
