<?php

namespace App\QueryBuilder\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class KmlActiveFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->where('active', $value);
    }
}
