<?php

namespace App\QueryBuilder\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class UserCreatedAtFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->whereDate('created_at', $value);
    }
}
