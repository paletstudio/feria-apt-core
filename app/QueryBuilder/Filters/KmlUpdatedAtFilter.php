<?php

namespace App\QueryBuilder\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class AdviceDateFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->whereDate('updated_at', $value);
    }
}
