<?php

namespace App\QueryBuilder\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class PublicityDescriptionFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->where('descripcion', 'ilike', "%$value%");
    }
}
