<?php

namespace App\QueryBuilder\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class RateActiveFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->where('activo', $value);
    }
}
