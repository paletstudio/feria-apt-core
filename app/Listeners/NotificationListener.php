<?php

namespace App\Listeners;

use App\Events\NotificationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NotificationListener
{

    /**
     * Handle the event.
     *
     * @param  NotificationEvent  $event
     * @return void
     */
    public function handle(NotificationEvent $event)
    {
        try {
            if (config('services.onesignal.enabled')) {
                $headings = ["en" =>  $event->title];
                $content = ["en" =>  $event->message];

                $fields = [
                    'app_id' => config('services.onesignal.app_id'),
                    'headings' => $headings,
                    'contents' => $content,
                ];
                if (config('services.onesignal.mode') === 'DEV') {
                    $fields['include_player_ids'] = ['9db5c5aa-8a1c-4a9a-a14d-4da8ce2aba1d']; //movera al segmento de tester users
                } else if (config('services.onesignal.enabled') === 'PROD') {
                    $fields['included_segments'] = ['All'];
                }

                if ($event->subtitle) {
                    $fields['subtitle'] = ["en" =>  $event->subtitle];
                }

                $fields = json_encode($fields);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json; charset=utf-8',
                    'Authorization: Basic ' . config('services.onesignal.token')
                ]);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                Log::info("response", ['d' => $response]);

                curl_close($ch);
            }
        } catch (\Throwable $th) {
            Log::info("Error notificacion push", ['error' => $th]);
        }
    }
}
