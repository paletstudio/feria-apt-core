<?php

namespace App\Presenters;

use AdditionApps\FlexiblePresenter\FlexiblePresenter;

class ZonePresenter extends FlexiblePresenter
{
    public function values(): array
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'ancho' => $this->alto,
            'alto' => $this->alto,
            'costo' => $this->costo,
            'activo' => $this->activo,
        ];
    }
}
