<?php

namespace App\Presenters;

use AdditionApps\FlexiblePresenter\FlexiblePresenter;

class PublicityPresenter extends FlexiblePresenter
{
    public function values(): array
    {
        return [
            'id' => $this->id,
            'descripcion' => $this->descripcion,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'banner' => $this->banner,
            'activo' => $this->activo,
            'created_at' => $this->created_at,
            'link' => $this->link,
            'zona_id' => $this->zona_id,
            'zone' => ZonePresenter::make($this->whenLoaded('zone'))
        ];
    }
}
