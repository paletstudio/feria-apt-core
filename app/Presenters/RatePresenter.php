<?php

namespace App\Presenters;

use AdditionApps\FlexiblePresenter\FlexiblePresenter;

class RatePresenter extends FlexiblePresenter
{
    public function values(): array
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'tipo' => $this->tipo,
            'ac' => $this->ac,
            'precio' => $this->precio,
            'activo' => $this->activo,
            'fecha_tarifa' => $this->fecha_tarifa,
            'fecha_fin' => $this->fecha_fin,
        ];
    }
}
