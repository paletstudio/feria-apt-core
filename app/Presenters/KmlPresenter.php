<?php

namespace App\Presenters;

use AdditionApps\FlexiblePresenter\FlexiblePresenter;

class KmlPresenter extends FlexiblePresenter
{
    public function values(): array
    {
        return [
            'id' => $this->id,
            'route_name' => $this->route_name,
            'id_kml' => $this->id_kml,
            'url' => $this->url,
            'active' => $this->active,
            'updated_at' => $this->updated_at,
            'geojson' => $this->geojson ?  json_decode($this->geojson, true) : null,
        ];
    }
}
