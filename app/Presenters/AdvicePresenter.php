<?php

namespace App\Presenters;

use AdditionApps\FlexiblePresenter\FlexiblePresenter;

class AdvicePresenter extends FlexiblePresenter
{
    public function values(): array
    {
        return [
            'id' => $this->id,
            'titulo' => $this->titulo,
            'subtitulo' => $this->subtitulo,
            'descripcion' => $this->descripcion,
            'fecha' => $this->fecha,
            'activo' => $this->activo,
            'imagen' => $this->imagen,
        ];
    }
}
