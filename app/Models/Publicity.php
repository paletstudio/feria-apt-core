<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

class Publicity extends Model
{
	use SoftDeletes;

	protected $table = 'publicidad';
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
	protected $fillable = ['descripcion', 'fecha_inicio', 'fecha_fin', 'link', 'zona_id', 'activo', 'banner'];
	protected $dates = ['deleted_at'];

	public function zone(): BelongsTo
	{
		return $this->belongsTo(Zone::class, 'zona_id');
	}

	public function getBannerAttribute($value)
	{
		if (!$value || strrpos($value, "http") !== false) return $value;
		return url(Storage::url($value));
	}
}
