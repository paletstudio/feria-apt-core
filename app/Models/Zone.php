<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
	protected $table = 'zona';
	protected $hidden = ['created_at', 'updated_at'];
	protected $fillable = ['nombre', 'descripcion', 'ancho', 'alto', 'costo', 'activo'];

	public function publicity()
	{
		return $this->hasMany('App\Publicity', 'zona_id');
	}
}
