<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;

class Advice extends Model
{
	use SoftDeletes;

	protected $table = 'avisos';
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
	protected $fillable = ['titulo', 'subtitulo', 'descripcion', 'fecha', 'activo', 'imagen'];
	protected $dates = ['deleted_at'];

	public function getImagenAttribute($value)
	{
		if (!$value || strrpos($value, "http") !== false) return $value;
		return url(Storage::url($value));
	}
}
