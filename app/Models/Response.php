<?php

namespace App\Models;

class Response
{
	public $code;
	public $data;
	public $message;
	public $exception;

	public function __construct()
	{
		$this->code = 200;
		$this->data = [];
		$this->message = '';
		$this->exception = '';
	}
}
