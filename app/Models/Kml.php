<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property integer $id
 * @property string $route_name
 * @property string $description
 * @property string $url
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 */
class Kml extends Model
{
    use HasFactory;

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['route_name', 'description', 'url', 'active', 'created_at', 'updated_at', 'geojson'];

    /**
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'geojson' => 'json'
    ];

    public function getUrlAttribute($value)
    {
        if (!$value || strrpos($value, "http") !== false) return $value;
        return url(Storage::url($value));
    }

    // public function getGeojsonAttribute($value)
    // {
    //     return json_decode($value, true);
    // }

    // public function setGeojsonAttribute($value)
    // {
    //     return json_encode($value, JSON_NUMERIC_CHECK);
    // }
}
