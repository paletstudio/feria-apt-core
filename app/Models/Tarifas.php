<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Tarifas extends Model
{
	use SoftDeletes;

	protected $table = 'tarifas';
	protected $fillable = ['nombre', 'tipo', 'ac', 'precio', 'activo', 'fecha_tarifa', 'fecha_fin'];
	protected $dates = ['deleted_at'];

	/**
	 * @var array
	 */
	protected $casts = [
		'ac' => 'boolean',
		'activo' => 'boolean',
	];
}
