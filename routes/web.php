<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Home');
})->middleware('auth');

Route::get('kmls/create_masive', [App\Http\Controllers\KmlController::class, 'createMasive'])->name('kmls.create.masive')->middleware('auth');
Route::post('kmls/create_masive', [App\Http\Controllers\KmlController::class, 'storeMasive'])->name('kmls.store.masive')->middleware('auth');
Route::resource('advices', App\Http\Controllers\AdviceController::class)->middleware('auth')->except(['destroy']);
Route::resource('kmls', App\Http\Controllers\KmlController::class)->middleware('auth')->except(['destroy']);
Route::resource('publicity', App\Http\Controllers\PublicityController::class)->middleware('auth')->except(['destroy']);
Route::resource('zones', App\Http\Controllers\ZoneController::class)->middleware('auth')->except(['destroy', 'create']);
Route::resource('version', App\Http\Controllers\VersionController::class)->middleware('auth')->except(['destroy', 'create', 'edit']);
Route::resource('rates', App\Http\Controllers\RateController::class)->middleware('auth')->except(['destroy']);
Route::get('notifications', [App\Http\Controllers\NotificationsController::class, 'index'])->name('notifications.index')->middleware('auth');
Route::post('notifications', [App\Http\Controllers\NotificationsController::class, 'sendNotification'])->name('notifications.send')->middleware('auth');
Route::resource('users', App\Http\Controllers\UserController::class)->middleware('auth')->except(['destroy']);
