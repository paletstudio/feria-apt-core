<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\KmlController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('kmls', [App\Http\Controllers\Api\KmlController::class, 'index']);
Route::get('advice', [App\Http\Controllers\Api\AdviceController::class, 'index']);
Route::get('rate/getactive', [App\Http\Controllers\Api\RateController::class, 'activeRates']);
Route::get('publicity/getactive', [App\Http\Controllers\Api\PublicityController::class, 'getactive']);
Route::get('kmls/can', [App\Http\Controllers\Api\KmlController::class, 'canKmls']);

// Route::get('zone', 'ZoneController@index');
// Route::get('rate/getactive', 'TarifasController@activeRates');
// Route::get('publicity/getactive', 'PublicityController@activePublicity');
// Route::get('zone/{id}', 'ZoneController@show');
// Route::get('version', 'VersionController@get');

// Route::get('/kmls', [KmlsController::class, 'index']);
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::get('advice', [App\Http\Controllers\NotificationsController::class, 'index']);
// Route::get('rate', [App\Http\Controllers\RateController::class, 'index']);
// Route::get('publicity', [App\Http\Controllers\PublicityController::class, 'index']);
// Route::get('zone', [App\Http\Controllers\ZoneController::class, 'index']);
// Route::get('rate/getactive', [App\Http\Controllers\RateController::class, 'index']); //falata agregar actvios
// Route::get('publicity/getactive', [App\Http\Controllers\PublicityController::class, 'index']); //falata agregar actvios
// Route::get('zone/{zoneId}', [App\Http\Controllers\ZoneController::class, 'show']);
// Route::get('version', [App\Http\Controllers\ZoneController::class, 'index']);
// Route::get('kmls', [App\Http\Controllers\NotificationsController::class, 'index']);
