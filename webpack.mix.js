const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .extract()
  .ts('resources/js/app.tsx', 'public/js')
  .react()
  .postCss('resources/css/app.css', 'public/css')
  .copyDirectory('resources/images', 'public/images')
  .alias({
    ziggy: path.resolve('vendor/tightenco/ziggy/dist'),
    '@common': path.resolve('resources/js/common'),
  })
  .options({
    hmrOptions: {
      host: 'localhost',
      port: 8081,
    },
  });
mix.webpackConfig({
  devServer: {
    host: '0.0.0.0',
    port: 8081,
  },
});
