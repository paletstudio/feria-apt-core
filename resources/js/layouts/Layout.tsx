import React, { useEffect, useState } from 'react';
import { Divider, Toolbar, useMediaQuery, IconButton, Typography, Box, Drawer, Paper } from '@mui/material';
import { useTheme, styled } from '@mui/material/styles';
import MenuListItems from './components/MenuListItems';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import MenuIcon from '@mui/icons-material//Menu';
import { usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
import { DefaultPageProps } from '../types';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Breadcrumbs from '@common/components/Breadcrumbs';
import BackButton from '@common/components/BackButton';

const drawerWidth = 240;

interface Props {
  window?: () => Window;
  title: string;
}

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, { shouldForwardProp: (prop) => prop !== 'open' })<AppBarProps>(({ theme, open }) => ({
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },

    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  padding: theme.spacing(3),
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginLeft: `-${drawerWidth}px`,
  ...(open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  }),
  backgroundColor: theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900],
  height: '100vh',
  overflow: 'auto',
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const Layout: React.FC<Props> = ({ window, children, title }) => {
  const { user } = usePage<Page<DefaultPageProps>>().props;
  const theme = useTheme();
  const [drawerOpen, setDrawerOpen] = useState(true);
  const isXsDown = useMediaQuery(theme.breakpoints.down('xs'));

  useEffect(() => {
    if (isXsDown) {
      setDrawerOpen(false);
    }
  }, [isXsDown]);

  const handleDrawerToggle = () => {
    setDrawerOpen(!drawerOpen);
  };

  const container = window !== undefined ? () => window().document.body : undefined;

  const drawer = (
    <>
      <DrawerHeader>
        <IconButton
          sx={{
            color: '#FFFFFF',
          }}
          onClick={handleDrawerToggle}
        >
          <ChevronLeftIcon />
        </IconButton>
      </DrawerHeader>

      <MenuListItems user={user} />
    </>
  );

  return (
    <Box sx={{ display: 'flex' }}>
      <AppBar
        sx={{
          backgroundColor: '#2397cb',
        }}
        position="fixed"
        open={drawerOpen}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{
              marginRight: '36px',
              color: '#FFFFFF',
              ...(drawerOpen && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Box
            component="img"
            sx={{
              height: 64,
            }}
            alt="feria logo"
            src="/images/icono_feria.png"
          />
          <Typography component="h1" variant="h6" noWrap sx={{ flexGrow: 1, color: 'white' }}>
            Sistema de avisos, publicidad y tarifas
          </Typography>
        </Toolbar>
      </AppBar>

      <Box component="nav" sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          open={drawerOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {drawer}
        </Drawer>
        <Drawer
          variant="persistent"
          sx={{
            display: { xs: 'none', sm: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
          open={drawerOpen}
        >
          {drawer}
        </Drawer>
      </Box>

      <Main open={drawerOpen}>
        <DrawerHeader />
        <Typography
          variant="h5"
          color="inherit"
          noWrap
          sx={{
            marginBottom: 1,
          }}
        >
          {title}
        </Typography>
        <Breadcrumbs />
        <Divider
          sx={{
            marginBottom: 2,
            marginTop: 2,
          }}
        />
        <BackButton />
        {children}
      </Main>
    </Box>
  );
};
export default Layout;
