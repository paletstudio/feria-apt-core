import React from 'react';
import { List, ListItem, ListItemIcon, ListItemText, Divider, Typography, Box } from '@mui/material';
import LogoutIcon from '@mui/icons-material/PowerSettingsNew';
import { InertiaLink } from '@inertiajs/inertia-react';
// import { makeStyles } from '@material-ui/core/styles';
import { User } from '../../types';
import { Inertia } from '@inertiajs/inertia';
// @ts-ignore
import route from 'ziggy';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import StreetviewIcon from '@mui/icons-material/Streetview';
import StorefrontIcon from '@mui/icons-material/Storefront';
import AppShortcutIcon from '@mui/icons-material/AppShortcut';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import CodeIcon from '@mui/icons-material/Code';
import SupervisedUserCircle from '@mui/icons-material/SupervisedUserCircle';

const options = [
  {
    id: 'Inicio',
    children: [
      {
        id: 'Avisos',
        icon: <NotificationsActiveIcon />,
        active: false,
        link: '/advices',
      },
      {
        id: 'Zonas',
        icon: <AppShortcutIcon />,
        active: false,
        link: '/zones',
      },
      {
        id: 'Kmls',
        icon: <StreetviewIcon />,
        active: false,
        link: '/kmls',
      },
      {
        id: 'Publicidad',
        icon: <StorefrontIcon />,
        active: false,
        link: '/publicity',
      },
      {
        id: 'Tarifas',
        icon: <AttachMoneyIcon />,
        active: false,
        link: '/rates',
      },
      {
        id: 'Notificaciones',
        icon: <NotificationsActiveIcon />,
        active: false,
        link: '/notifications',
      },
      {
        id: 'Usuarios',
        icon: <SupervisedUserCircle />,
        active: false,
        link: '/users',
      },
      {
        id: 'Versión',
        icon: <CodeIcon />,
        active: false,
        link: '/version',
      },
    ],
  },
];

interface Props {
  user: User;
}

const MenuListItems: React.FC<Props> = ({ user }) => {
  return (
    <>
      <Box justifyContent="center" alignItems="center" height={100} display="flex">
        <img
          src="/images/logo.png"
          style={{
            width: 100,
          }}
        />
      </Box>
      {user && user.name ? (
        <Box
          sx={{
            p: 2,
            color: 'white',
          }}
        >
          <Typography
            sx={{
              fontFamily: 'inherit',
              textAlign: 'center',
              fontWeight: 'bold',
            }}
          >{`Hola, ${user.name}`}</Typography>
        </Box>
      ) : null}

      <Divider sx={{ background: 'white', m: 2 }} />
      <List
        sx={{
          my: 0,
          mx: 1,
        }}
        disablePadding
      >
        {options.map(({ id, children }) => (
          <React.Fragment key={id}>
            <ListItem
              sx={{
                pt: 2,
              }}
            >
              <ListItemText
                sx={{
                  color: 'white',
                }}
              >
                {id}
              </ListItemText>
            </ListItem>
            {children.map(({ id: childId, icon, active, link }) => (
              <ListItem
                key={childId}
                button
                sx={[
                  {
                    py: 1 / 2,
                    borderRadius: '10px',
                    '&:hover,&:focus': {
                      backgroundColor: 'rgba(14, 60, 111, 0.2)',
                      color: 'primary.main',
                    },
                  },
                  active
                    ? {
                        color: '#4fc3f7',
                      }
                    : {},
                ]}
                component={InertiaLink}
                href={link}
              >
                <ListItemIcon
                  sx={{
                    minWidth: 'auto',
                    mr: 2,
                    color: 'white',
                  }}
                >
                  {icon}
                </ListItemIcon>
                <ListItemText
                  sx={{
                    color: 'white',
                  }}
                >
                  {childId}
                </ListItemText>
              </ListItem>
            ))}
            <Divider
              sx={{
                my: 2,
                mx: 0,
              }}
            />
            <ListItem
              button
              sx={{
                py: 1 / 2,
                borderRadius: '10px',
                '&:hover,&:focus': {
                  backgroundColor: 'rgba(14, 60, 111, 0.2)',
                  color: 'primary.main',
                },
              }}
              onClick={() => {
                Inertia.post(route('logout'));
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 'auto',
                  mr: 2,
                  color: 'white',
                }}
              >
                <LogoutIcon />
              </ListItemIcon>
              <ListItemText
                sx={{
                  color: 'white',
                }}
              >
                Cerrar sesión
              </ListItemText>
            </ListItem>
          </React.Fragment>
        ))}
      </List>
    </>
  );
};

export default MenuListItems;
