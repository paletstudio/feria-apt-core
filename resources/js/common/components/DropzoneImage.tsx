import React, { useState, useEffect } from 'react';
import Dropzone from 'react-dropzone';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';

const HEIGTH = 200;
const WIDTH = 150;

interface IProps {
  getCurrentImage: (image: any) => void;
  file: any;
  title: string;
  disabled?: boolean;
  label?: string;
  error?: boolean;
  helperText?: string;
}

function DropzoneImage({ disabled = false, title, getCurrentImage, file, error, helperText }: IProps) {
  const [currentFileFormat, setCurrentFileFormat] = useState<any>(null);

  const isValidHttpUrl = (image: any) => {
    try {
      if (image) {
        let type = typeof image;
        if (type === 'string') {
          return image;
        } else if (type === 'object') {
          return URL.createObjectURL(image);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getImage = (image: any) => {
    getCurrentImage && getCurrentImage(image);
  };

  useEffect(() => {
    if (file) {
      let format = isValidHttpUrl(file);
      setCurrentFileFormat(format);
    } else {
      setCurrentFileFormat(null);
    }
  }, [file]);

  return (
    <FormControl fullWidth variant="outlined" margin="dense">
      <InputLabel
        shrink
        style={{
          marginBottom: 100,
        }}
      >
        Imagen
      </InputLabel>
      <Box
        sx={{
          marginTop: 1,
        }}
      >
        <Dropzone onDrop={(files) => getImage(files)} accept="image/*" maxFiles={1} multiple={false}>
          {({ getRootProps, getInputProps }) => (
            <Paper
              variant="outlined"
              square
              style={{
                margin: 'auto',
                overflow: 'hidden',
                height: HEIGTH ? HEIGTH + 7 : 200,
                width: '100%',
              }}
              {...getRootProps({
                className: 'dropzone',
                onDrop: (event) => event.stopPropagation(),
              })}
            >
              <Box display="flex" justifyContent="center" alignItems="center">
                <div
                  style={{
                    // borderStyle: 'dashed',
                    height: HEIGTH ? HEIGTH + 5 : 200,
                    // borderColor: '#2a4633',
                    width: WIDTH ? WIDTH : 150,
                  }}
                >
                  {!disabled && <input {...getInputProps()} />}
                  {currentFileFormat ? (
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-around',
                      }}
                    >
                      <img
                        src={currentFileFormat}
                        style={{
                          height: HEIGTH ? HEIGTH : 444,
                          // width: 300,
                          textAlign: 'center',
                        }}
                      />
                    </div>
                  ) : (
                    <Box display="flex" justifyContent="center" alignItems="center">
                      <p
                        style={{
                          textAlign: 'center',
                          fontSize: 10,
                          marginTop: 100,
                        }}
                      >
                        {title}
                      </p>
                    </Box>
                  )}
                </div>
              </Box>
            </Paper>
          )}
        </Dropzone>
      </Box>
      <FormHelperText error={error}>{helperText}</FormHelperText>
    </FormControl>
  );
}

export default DropzoneImage;
