import React, { useEffect, useState } from 'react';
import { useTable, Column, usePagination, useSortBy, useFilters, TableState } from 'react-table';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter,
  TablePagination,
  makeStyles,
  TableSortLabel,
  Toolbar,
  IconButton,
  Popover,
  Typography,
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  Switch,
  Button,
  Tooltip,
} from '@mui/material';
import clsx from 'clsx';
import { ViewColumn as ViewColumnIcon } from '@mui/icons-material';
import { useDebouncedEffect, usePrevious } from '@common/utils';
import DefaultColumnFilter from './filters/DefaultFilter';
import qs from 'qs';
import { usePopupState, bindTrigger, bindPopover } from 'material-ui-popup-state/hooks';
import TableActionsComponent from './TableActionsComponent';

interface Props {
  columns: Array<Column<any>>;
  data: Array<any>;
  currentPage: number;
  totalCount: number;
  onChange: (data: { page: number; perPage: number; sort?: string; filters?: { [key: string]: any } }) => void;
  additionalColumns?: Array<Column<any>>;
  sortingDisabled?: boolean;
  currentPerPage: number;
  renderToolbar?: React.ReactNode;
  toolbarHidden?: boolean;
}

const DataTable: React.FC<Props> = ({
  columns,
  data,
  currentPage,
  currentPerPage,
  totalCount,
  onChange,
  additionalColumns,
  sortingDisabled,
  renderToolbar,
  toolbarHidden,
}) => {
  // const classes = useStyles();
  const popupState = usePopupState({
    variant: 'popover',
    popupId: 'hideColumnsPopover',
  });

  const triggerOnChange = (page: number = currentPage, perPage: number = currentPerPage) => {
    let defaultFilters: { [key: string]: any } = {};

    onChange({
      page,
      perPage,
      sort: sortBy.map((s) => `${s.desc ? '-' : ''}${s.id}`).join(','),
      filters: filters.reduce((prev, next) => {
        prev[next.id] = next.value;
        return prev;
      }, defaultFilters),
    });
  };

  const defaultColumn: Partial<Column<object>> = React.useMemo(() => {
    return {
      width: 'auto',
      minWidth: 50,
      Filter: DefaultColumnFilter,
    };
  }, []);

  const initialState: Partial<TableState> = React.useMemo(() => {
    const queryString = qs.parse(window.location.search.replace('?', ''));
    console.log(queryString);

    let defaultFilters: Array<{ id: string; value: any }> = [];
    let defaultSortBy: Array<{ id: string; desc: boolean }> = [];

    if (queryString.filter) {
      Object.keys(queryString.filter).forEach((q) => {
        defaultFilters.push({
          id: q,
          // @ts-ignore
          value: queryString.filter[q],
        });
      });
    }

    if (queryString.sort) {
      const parts = queryString.sort.toString().split('-');

      defaultSortBy.push({
        id: parts.length > 1 ? parts[1] : parts[0],
        desc: parts.length > 1 ? true : false,
      });
    }

    return {
      pageIndex: currentPage,
      filters: defaultFilters,
      sortBy: defaultSortBy,
    };
  }, []);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    visibleColumns,
    allColumns,
    toggleHideAllColumns,
    state: { pageIndex, sortBy, filters },
  } = useTable(
    {
      columns,
      data,
      useControlledState: (state) => {
        return React.useMemo(
          () => ({
            ...state,
            pageIndex: currentPage,
          }),
          [state, currentPage]
        );
      },
      initialState, // Pass our hoisted table state
      manualPagination: true,
      pageCount: totalCount,
      defaultColumn,
      manualSortBy: true,
      manualFilters: true,
      disableSortBy: true,
      autoResetFilters: false,
    },
    useFilters,
    useSortBy,
    usePagination,
    (hooks) => {
      if (additionalColumns) {
        hooks.visibleColumns.push((columns) => [...columns, ...additionalColumns]);
      }
    }
    // useResizeColumns,
    // useGridLayout
    // useBlockLayout,
    // useFlexLayout
  );

  const prevFilters = usePrevious(filters);
  const prevSortBy = usePrevious(sortBy);

  useDebouncedEffect(
    () => {
      if (prevSortBy !== undefined || prevFilters !== undefined) {
        triggerOnChange();
      }
    },
    500,
    [filters, sortBy]
  );

  return (
    <>
      {toolbarHidden ? null : (
        <Toolbar
          disableGutters
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            padding: `0px 8px`,
            minHeight: 50,
          }}
        >
          {renderToolbar ? renderToolbar : null}
          <Tooltip title="Mostrar/ocultar columnas">
            <IconButton
              sx={{
                padding: 0.5,
              }}
              {...bindTrigger(popupState)}
            >
              <ViewColumnIcon />
            </IconButton>
          </Tooltip>
          <Popover
            {...bindPopover(popupState)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
          >
            <div
              style={{
                padding: `16px 16px 8px`,
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              <FormControl component="fieldset">
                <FormLabel component="legend">
                  <Typography variant="caption">Mostrar/ocultar columnas</Typography>
                </FormLabel>
                <FormGroup>
                  {allColumns.map((column) => (
                    <FormControlLabel
                      key={column.id}
                      control={<Switch color="primary" {...column.getToggleHiddenProps()} />}
                      label={<Typography variant="body2">{column.Header}</Typography>}
                    />
                  ))}
                </FormGroup>
              </FormControl>
              <Button
                onClick={() => {
                  toggleHideAllColumns(false);
                }}
                color="primary"
                style={{ marginTop: 8 }}
              >
                Mostrar todas
              </Button>
            </div>
          </Popover>
        </Toolbar>
      )}

      <Table
        stickyHeader
        {...getTableProps([
          {
            // @ts-ignore
            sx: {
              tableLayout: 'fixed',
            },
          },
        ])}
      >
        <colgroup>
          {visibleColumns.map((column, index) => (
            <col key={index} style={{ width: column.width }}></col>
          ))}
        </colgroup>
        <TableHead>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <TableCell {...column.getHeaderProps()}>
                  <div {...column.getSortByToggleProps()}>
                    {column.render('Header')}

                    {column.canSort ? (
                      <TableSortLabel
                        active={column.isSorted}
                        // react-table has a unsorted state which is not treated here
                        direction={column.isSortedDesc ? 'desc' : 'asc'}
                      />
                    ) : null}
                  </div>

                  {/* Render the columns filter UI */}
                  <div>{column.canFilter ? column.render('Filter') : null}</div>

                  {/* <div
                                            {...column.getResizerProps()}
                                            className={clsx(
                                                classes.resizer
                                                //     , {
                                                //     classes.isResizing: column.isResizing
                                                // }
                                            )}
                                        /> */}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);
            return (
              <TableRow {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <TableCell
                      {...cell.getCellProps([
                        {
                          // @ts-ignore
                          sx: {
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                          },
                        },
                      ])}
                    >
                      {cell.render('Cell')}
                    </TableCell>
                  );
                })}
              </TableRow>
            );
          })}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[10, 25, 50]}
              colSpan={visibleColumns.length}
              count={totalCount}
              rowsPerPage={+currentPerPage}
              page={pageIndex - 1}
              SelectProps={{
                inputProps: {
                  'aria-label': 'rows per pa',
                },
              }}
              // @ts-ignore
              onPageChange={(event, newPage) => {
                triggerOnChange(newPage + 1);
              }}
              onRowsPerPageChange={(e: any) => {
                triggerOnChange(1, +e.target.value);
              }}
              // @ts-ignore
              ActionsComponent={TableActionsComponent}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </>
  );
};

DataTable.defaultProps = {
  sortingDisabled: false,
  toolbarHidden: false,
};

export default DataTable;
