import React from 'react';
import { Pagination } from '@mui/material';

interface TableActionsProps {
  count: number;
  rowsPerPage: number;
  page: number;
  onPageChange: (event: any, page: number) => void;
}

const TableActionsComponent: React.FC<TableActionsProps> = ({ count, rowsPerPage, page, onPageChange }) => {
  return (
    <Pagination
      sx={{
        flexShrink: 0,
        marginLeft: 2,
      }}
      count={Math.ceil(count / rowsPerPage)}
      page={page + 1}
      onChange={(event, newPage) => {
        onPageChange(event, newPage - 1);
      }}
      showFirstButton
      showLastButton
    />
  );
};

export default TableActionsComponent;
