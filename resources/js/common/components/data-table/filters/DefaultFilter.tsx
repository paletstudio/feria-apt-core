import { IconButton, InputAdornment, TextField } from '@mui/material';
import React from 'react';
import { FilterProps, Renderer } from 'react-table';
import { Clear as ClearIcon } from '@mui/icons-material';

const DefaultColumnFilter: Renderer<FilterProps<any>> = ({
    column: { filterValue, setFilter, Header },
}) => {
    return (
        <TextField
            value={filterValue || ''}
            onChange={(e) => {
                setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
            }}
            placeholder="Filtro"
            autoComplete="off"
            InputProps={{
                endAdornment: filterValue ? (
                    <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password visibility"
                            onClick={() => {
                                setFilter(undefined);
                            }}
                        >
                            <ClearIcon />
                        </IconButton>
                    </InputAdornment>
                ) : null,
            }}
        />
    );
};

export default DefaultColumnFilter;
