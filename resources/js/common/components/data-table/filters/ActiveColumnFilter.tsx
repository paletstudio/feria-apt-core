import React from 'react';
import { FilterProps, Renderer } from 'react-table';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const ActiveColumnFilter: Renderer<FilterProps<any>> = ({ column: { filterValue, setFilter } }) => {
    return (
        <FormControl
            sx={{
                margin: 1,
                minWidth: '120px',
            }}
        >
            <Select
                value={filterValue || null}
                onChange={(e) => {
                    setFilter(e.target.value);
                }}
                displayEmpty
                inputProps={{ 'aria-label': 'Without label' }}
            >
                <MenuItem value="">Ninguno</MenuItem>
                <MenuItem value="1">Activo</MenuItem>
                <MenuItem value="0">No Activo</MenuItem>
            </Select>
        </FormControl>
    );
};

export default ActiveColumnFilter;
