import { InputAdornment } from '@mui/material';
import React from 'react';
import { FilterProps, Renderer } from 'react-table';
import { CalendarToday as CalendarIcon } from '@mui/icons-material';
import TextField from '@mui/material/TextField';
import MobileDatePicker from '@mui/lab/MobileDatePicker';

const DateColumnFilter: Renderer<FilterProps<any>> = ({
  // @ts-ignore
  column: { filterValue, setFilter },
}) => {
  return (
    <MobileDatePicker
      clearText="Limpiar"
      cancelText="Cancelar"
      clearable
      value={filterValue || null}
      onChange={(date: any) => {
        // setFilter(date ? date.format('YYYY-MM-DD') : undefined);
      }}
      onAccept={(date: any) => {
        setFilter(date ? date.format('YYYY-MM-DD') : undefined);
      }}
      inputFormat="YYYY-MM-DD"
      toolbarTitle="Elige una fecha"
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <CalendarIcon style={{ fontSize: 20 }} />
          </InputAdornment>
        ),
      }}
      renderInput={(params) => <TextField {...params} />}
    />
  );
};

export default DateColumnFilter;
