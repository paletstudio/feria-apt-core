import React from 'react';
import { FormControl, MenuItem, Select, makeStyles } from '@mui/material';
import { ColumnInstance } from 'react-table';

interface Props {
    column: ColumnInstance<any>;
    options: Array<{ label: string; value: string | number | undefined }>;
}

const SelectColumnFilter: React.FC<Props> = ({ column: { filterValue, setFilter }, options }) => {
    return (
        <FormControl
            sx={{
                minWidth: 120,
            }}
        >
            <Select
                value={filterValue || ''}
                onChange={(e) => {
                    setFilter(e.target.value);
                }}
                placeholder="Elige una opción"
                displayEmpty
                size="small"
            >
                {options.map((option) => (
                    <MenuItem key={option.label} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
};

export default SelectColumnFilter;
