import React, { useEffect, useState, useCallback, useMemo } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import ReactLeafletKml from 'react-leaflet-kml';
import 'leaflet/dist/leaflet.css';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';

const center: any = [25.7002764, -100.2503261];
const zoom: number = 11;

interface IProps {
  children?: React.ReactChild;
}

const LeafletMap: React.FC<IProps> = ({ children }) => {
  return (
    <FormControl fullWidth variant="outlined" margin="dense">
      <InputLabel
        shrink
        style={{
          marginBottom: 100,
        }}
      >
        Ruta de archivo KML
      </InputLabel>
      <Box
        sx={{
          marginTop: 1,
        }}
      >
        <MapContainer
          center={center}
          zoom={zoom}
          scrollWheelZoom={false}
          style={{
            width: '100%',
            height: 400,
          }}
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {children}
        </MapContainer>
      </Box>
    </FormControl>
  );
};

export default LeafletMap;
