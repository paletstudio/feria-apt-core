import * as React from 'react';
import { emphasize, styled } from '@mui/material/styles';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Chip from '@mui/material/Chip';
import HomeIcon from '@mui/icons-material/Home';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { InertiaLink } from '@inertiajs/inertia-react';
import Typography from '@mui/material/Typography';

const breadcrumbNameMap: {
  [key: string]: string;
} = {
  '/': 'Inicio',
  '/advices': 'Avisos',
  '/advices/create': 'Nuevo',
  '/zones': 'Zonas',
  '/zones/create': 'Nuevo',
  '/kmls': 'KMLs',
  '/kmls/create': 'Nuevo',
  '/publicity': 'Publicidad',
  '/publicity/create': 'Nuevo',
  '/rates': 'Tarifas',
  '/rates/create': 'Nuevo',
  '/version': 'Versión',
  '/notifications': 'Notificaciones',
  '/users': 'Usuarios',
  '/users/create': 'Nuevo',
};

const StyledBreadcrumb = styled(Chip)(({ theme }) => {
  const backgroundColor = theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[800];
  return {
    backgroundColor,
    height: theme.spacing(3),
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover, &:focus': {
      backgroundColor: emphasize(backgroundColor, 0.06),
    },
    '&:active': {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(backgroundColor, 0.12),
    },
  };
}) as typeof Chip; // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

const renderEditOrShow = (str: string) => {
  if (str === 'edit') {
    return 'Editar';
  } else if (str === 'show') {
    return 'Detalles';
  } else {
    return str;
  }
};

export default function CustomizedBreadcrumbs() {
  const pathnames = location.pathname.split('/').filter((x) => x);

  return (
    <Breadcrumbs aria-label="breadcrumb">
      {pathnames.map((value, index) => {
        const last = index === pathnames.length - 1;
        let to = `/${pathnames.slice(0, index + 1).join('/')}`;

        let href = to;

        return last ? (
          <StyledBreadcrumb key={index} label={renderEditOrShow(breadcrumbNameMap[to] || value)} />
        ) : (
          <StyledBreadcrumb
            key={index}
            component={InertiaLink}
            href={href}
            label={renderEditOrShow(breadcrumbNameMap[to] || value)}
          />
        );
      })}
    </Breadcrumbs>
  );
}
