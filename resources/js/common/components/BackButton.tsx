import * as React from 'react';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';

const breadcrumbNameMap: {
  [key: string]: string;
} = {
  '/': 'Inicio',
  '/advices': 'Avisos',
  '/zones': 'Zonas',
  '/kmls': 'KMLs',
  '/publicity': 'Publicidad',
  '/rates': 'Tarifas',
  '/version': 'Versión',
  '/notifications': 'Notificaciones',
  '/users': 'Usuarios',
};

export default function BackButton() {
  const pathname = window.location.pathname;

  if (breadcrumbNameMap[pathname] === undefined) {
    return (
      <Stack
        direction="row"
        sx={{
          marginBottom: 2,
          marginTop: 2,
        }}
      >
        <Button
          onClick={() => window.history.back()}
          size="small"
          variant="contained"
          startIcon={<KeyboardBackspaceIcon />}
        >
          Regresar
        </Button>
      </Stack>
    );
  }
  return null;
}
