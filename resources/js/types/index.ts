export type User = {
  id?: number;
  name: string;
  email?: string;
  created_at?: string;
  updated_at?: string;
  password?: string;
};

export type DefaultPageProps = {
  user: User;
  currentRouteName: string;
};

export interface Customer {
  id?: number;
  name: string;
  active: boolean;
  sapKey: string;
  logo?: string;
}

export interface Advice {
  id?: number;
  titulo: string;
  subtitulo: string;
  descripcion: string;
  fecha: string;
  activo: boolean;
  imagen?: any;
}

export interface Kml {
  id?: number;
  route_name: string;
  active: boolean;
  url?: string;
  file?: any;
  updated_at?: string;
  id_kml?: string;
  geojson?: any;
}

export interface Publicity {
  id?: number;
  descripcion: string;
  fecha_inicio: string | null;
  fecha_fin: string | null;
  banner: any;
  activo: boolean;
  deleted_at?: string;
  created_at?: string;
  link?: string;
  zona_id: number | null;
  zone?: Zone;
}

export interface Zone {
  id?: number;
  nombre: string;
  descripcion: string;
  ancho: string;
  alto: string;
  costo: string;
  activo: boolean;
}

export interface Version {
  id?: number;
  version: string;
  date?: string;
}

export interface Rate {
  id?: number;
  nombre: string;
  tipo: number | null;
  ac: any;
  activo: boolean;
  precio?: number | null;
  fecha_tarifa?: string;
  fecha_fin?: string;
}
