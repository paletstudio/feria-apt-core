import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';
import { Zone, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
import { CKEditor } from '@ckeditor/ckeditor5-react';
// @ts-ignore
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';
import MobileDatePicker from '@mui/lab/MobileDatePicker';
import { CalendarToday as CalendarIcon } from '@mui/icons-material';
import NumberFormat from '@common/components/NumberFormat';

type PageProps = {
  data: Zone;
};

const RESOURCE = 'zones.update';

const Edit = () => {
  const { data: zoneProp } = usePage<Page<PageProps & DefaultPageProps>>().props;
  const { data, setData, patch, processing, errors, reset, transform } = useForm<Zone>({
    nombre: zoneProp.nombre,
    descripcion: zoneProp.descripcion,
    activo: zoneProp.activo,
    alto: zoneProp.alto,
    ancho: zoneProp.ancho,
    costo: zoneProp.costo,
    id: zoneProp.id,
  });
  const { enqueueSnackbar } = useSnackbar();

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    patch(route(RESOURCE, [data.id]), {
      onSuccess: () => {
        enqueueSnackbar('Zona actualizada exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <form onSubmit={submit}>
        <Paper
          sx={{
            padding: 2,
          }}
        >
          <Container maxWidth="sm">
            <TextField
              label="Nombre"
              value={data.nombre}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('nombre', e.target.value)}
              error={!!errors.nombre}
              helperText={errors.nombre ? errors.nombre : ''}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              label="Descripción"
              value={data.descripcion}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('descripcion', e.target.value)}
              error={!!errors.descripcion}
              helperText={errors.descripcion ? errors.descripcion : ''}
              InputLabelProps={{
                shrink: true,
              }}
            />

            <TextField
              label="Alto de banner"
              value={data.alto}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('alto', e.target.value)}
              error={!!errors.alto}
              helperText={errors.alto ? errors.alto : ''}
              InputLabelProps={{
                shrink: true,
              }}
              InputProps={{
                inputComponent: NumberFormat as any,
              }}
            />

            <TextField
              label="Ancho de banner"
              value={data.ancho}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('ancho', e.target.value)}
              error={!!errors.ancho}
              helperText={errors.ancho ? errors.ancho : ''}
              InputLabelProps={{
                shrink: true,
              }}
              InputProps={{
                inputComponent: NumberFormat as any,
              }}
            />

            <TextField
              label="Costo"
              value={data.costo}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('costo', e.target.value)}
              error={!!errors.costo}
              helperText={errors.costo ? errors.costo : ''}
              InputLabelProps={{
                shrink: true,
              }}
              InputProps={{
                inputComponent: NumberFormat as any,
                // decimalScale:
              }}
            />

            <FormControl component="fieldset" style={{ margin: 10 }}>
              <FormLabel component="legend">Activo</FormLabel>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Switch
                      color="primary"
                      checked={data.activo}
                      onChange={() => {
                        setData('activo', !data.activo);
                      }}
                    />
                  }
                  label={data.activo ? 'Si' : 'No'}
                />
              </FormGroup>
            </FormControl>
            <Box
              sx={{
                marginTop: 2,
                textAlign: 'center',
              }}
            >
              <Button type="submit" disabled={processing} variant="contained" color="primary">
                {processing ? <CircularProgress size={20} /> : 'Actualizar'}
              </Button>
            </Box>
          </Container>
        </Paper>
      </form>
    </div>
  );
};

Edit.layout = (page: any) => <Layout children={page} title="Editar zona" />;

export default Edit;
