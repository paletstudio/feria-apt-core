import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
} from '@mui/material';
import { DefaultPageProps, Zone } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
import { CKEditor } from '@ckeditor/ckeditor5-react';
// @ts-ignore
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import NumberFormat from '@common/components/NumberFormat';

type PageProps = {
  data: Zone;
};

const Show = () => {
  const { data } = usePage<Page<PageProps & DefaultPageProps>>().props;

  return (
    <div>
      <Paper
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <TextField
            label="Nombre"
            value={data.nombre}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            label="Descripción"
            value={data.descripcion}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            label="Alto de banner"
            value={data.alto}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              inputComponent: NumberFormat as any,
            }}
          />
          <TextField
            label="Ancho de banner"
            value={data.ancho}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              inputComponent: NumberFormat as any,
            }}
          />
          <TextField
            label="Costo"
            value={data.costo}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              inputComponent: NumberFormat as any,
            }}
          />
          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={<Switch color="primary" checked={data.activo} />}
                label={data.activo ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
        </Container>
      </Paper>
    </div>
  );
};

Show.layout = (page: any) => <Layout children={page} title="Detalles de zona" />;

export default Show;
