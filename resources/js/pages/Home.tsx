import { Typography } from '@mui/material';
import React from 'react';
import Layout from '../layouts/Layout';

const Welcome = ({}) => {
  return <Typography paragraph>Inicio</Typography>;
};

Welcome.layout = (page: any) => <Layout children={page} title="Inicio" />;

export default Welcome;
