import { Inertia, Page } from '@inertiajs/inertia';
import { usePage } from '@inertiajs/inertia-react';
import React from 'react';
import { CellProps, Column } from 'react-table';
import { User, DefaultPageProps } from 'resources/js/types';
import Layout from '../../layouts/Layout';
import {
  Edit as EditIcon,
  Visibility as VisibilityIcon,
  Add as AddIcon,
  ToggleOff as MuiToggleOff,
  ToggleOn as MuiToggleOn,
  Refresh as RefreshIcon,
} from '@mui/icons-material';
import { TableContainer, Paper, makeStyles, IconButton, Tooltip, Button, AccordionSummaryTypeMap } from '@mui/material';
// @ts-ignore
import route from 'ziggy';
import DataTable from '@common/components/data-table';
import DateColumnFilter from '@common/components/data-table/filters/DateColumnFilter';
import moment from 'moment';

const RESOURCE = 'users';

type PageProps = {
  data: {
    data: Array<User>;
    current_page: number;
    total: number;
    per_page: number;
  };
};

const Index = ({}) => {
  const { data: pagination } = usePage<Page<PageProps & DefaultPageProps>>().props;

  const data = React.useMemo(() => pagination.data, [pagination.data]);

  const columns: Array<Column<User>> = React.useMemo(
    () => [
      {
        Header: 'Nombre',
        accessor: 'name',
        // defaultCanSort: true,
      },
      {
        Header: 'Correo',
        accessor: 'email',
        // defaultCanSort: true,
      },

      {
        Header: 'Fecha de creación',
        accessor: 'created_at',
        Cell: ({ value }: { value: string }) => {
          if (value) {
            return moment(value).format('YYYY-MM-DD HH:mm:ss');
          }
          return '';
        },
        Filter: DateColumnFilter,
      },
    ],
    []
  );

  const onChangeTable = ({
    page,
    perPage,
    // @ts-ignore
    sort,
    filters,
  }: {
    page: number;
    perPage: number;
    sort?: string;
    filters?: { [key: string]: any };
  }) => {
    Inertia.get(
      route(`${RESOURCE}.index`),
      {
        page,
        per_page: perPage,
        // @ts-ignore
        sort,
        // @ts-ignore
        filter: filters,
      },
      {
        preserveState: true,
        preserveScroll: true,
        only: ['data'],
      }
    );
  };

  const additionalColumns: Array<Column<User>> = React.useMemo(
    () => [
      {
        id: 'actions',
        Header: 'Acciones',
        width: 100,
        Cell: ({ row }: React.PropsWithChildren<CellProps<User>>) => (
          <>
            <Tooltip title="Ver detalles">
              <IconButton
                sx={{
                  padding: 0.5,
                }}
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.show`, [row.original.id]));
                }}
              >
                <VisibilityIcon
                  sx={{
                    fontSize: '1.3rem',
                  }}
                />
              </IconButton>
            </Tooltip>
            <Tooltip title="Editar">
              <IconButton
                sx={{
                  padding: 0.5,
                }}
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.edit`, [row.original.id]));
                }}
              >
                <EditIcon
                  sx={{
                    fontSize: '1.3rem',
                  }}
                />
              </IconButton>
            </Tooltip>
          </>
        ),
      },
    ],
    []
  );

  return (
    <>
      <TableContainer component={Paper}>
        <DataTable
          columns={columns}
          data={data}
          currentPage={pagination.current_page}
          currentPerPage={pagination.per_page}
          totalCount={pagination.total}
          onChange={onChangeTable}
          additionalColumns={additionalColumns}
          renderToolbar={
            <>
              <Button
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.create`));
                }}
                sx={{
                  marginRight: 1,
                }}
                variant="outlined"
                color="primary"
                size="small"
                startIcon={<AddIcon />}
              >
                Nuevo
              </Button>
              <Tooltip title="Recargar resultados">
                <IconButton
                  sx={{
                    padding: 0.5,
                  }}
                  onClick={() => {
                    Inertia.reload();
                  }}
                >
                  <RefreshIcon />
                </IconButton>
              </Tooltip>
            </>
          }
        />
      </TableContainer>
    </>
  );
};

Index.layout = (page: any) => <Layout children={page} title="Usuarios" />;

export default Index;
