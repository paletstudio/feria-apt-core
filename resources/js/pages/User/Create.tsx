import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';
import { User, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';

const RESOURCE = 'users.store';

const Create = () => {
  const { data, setData, post, processing, errors, reset } = useForm<User>({
    name: '',
    email: '',
    password: '',
  });
  const { enqueueSnackbar } = useSnackbar();

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    post(route(RESOURCE), {
      onSuccess: () => {
        reset();
        enqueueSnackbar('Usuario creado exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <Paper
        onSubmit={submit}
        component="form"
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <TextField
            required
            label="Nombre"
            value={data.name}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('name', e.target.value)}
            error={!!errors.name}
            helperText={errors.name ? errors.name : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            required
            label="Correo"
            value={data.email}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('email', e.target.value)}
            error={!!errors.email}
            helperText={errors.email ? errors.email : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            required
            type="password"
            label="Contraseña"
            value={data.password}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('password', e.target.value)}
            error={!!errors.password}
            helperText={errors.password ? errors.password : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Box
            sx={{
              marginTop: 2,
              textAlign: 'center',
            }}
          >
            <Button type="submit" disabled={processing} variant="contained" color="primary">
              {processing ? <CircularProgress size={20} /> : 'Crear'}
            </Button>
          </Box>
        </Container>
      </Paper>
    </div>
  );
};

Create.layout = (page: any) => <Layout children={page} title="Crear usuario" />;

export default Create;

// export default Create;
