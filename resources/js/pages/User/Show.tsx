import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
} from '@mui/material';
import { DefaultPageProps, User } from '../../types';
import { usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';

type PageProps = {
  data: User;
};

const Show = () => {
  const { data } = usePage<Page<PageProps & DefaultPageProps>>().props;

  return (
    <div>
      <Paper
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <TextField
            label="Nombre"
            value={data.name}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            label="Correo"
            value={data.email}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Container>
      </Paper>
    </div>
  );
};

Show.layout = (page: any) => <Layout children={page} title="Detalles de usuario" />;

export default Show;
