import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';
import { User, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';

type PageProps = {
  data: User;
};

const RESOURCE = 'users.update';

const Edit = () => {
  const { data: userProp } = usePage<Page<PageProps & DefaultPageProps>>().props;
  const { data, setData, patch, processing, errors, reset, transform } = useForm<User>({
    name: userProp.name,
    email: userProp.email,
    id: userProp.id,
  });
  const { enqueueSnackbar } = useSnackbar();

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    patch(route(RESOURCE, [data.id]), {
      onSuccess: () => {
        enqueueSnackbar('Usuario actualizado exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <form onSubmit={submit}>
        <Paper
          sx={{
            padding: 2,
          }}
        >
          <Container maxWidth="sm">
            <TextField
              required
              label="Nombre"
              value={data.name}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('name', e.target.value)}
              error={!!errors.name}
              helperText={errors.name ? errors.name : ''}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              required
              label="Correo"
              value={data.email}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('email', e.target.value)}
              error={!!errors.email}
              helperText={errors.email ? errors.email : ''}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              type="password"
              label="Contraseña"
              value={data.password}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('password', e.target.value)}
              error={!!errors.password}
              helperText={errors.password ? errors.password : ''}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <Box
              sx={{
                marginTop: 2,
                textAlign: 'center',
              }}
            >
              <Button type="submit" disabled={processing} variant="contained" color="primary">
                {processing ? <CircularProgress size={20} /> : 'Actualizar'}
              </Button>
            </Box>
          </Container>
        </Paper>
      </form>
    </div>
  );
};

Edit.layout = (page: any) => <Layout children={page} title="Editar usuario" />;

export default Edit;
