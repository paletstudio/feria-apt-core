import React, { useEffect, useState } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';
import { Kml, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';
import Dropzone from '../../common/components/DropzoneFile';
import LeaftletMap from '../../common/components/LeaftletMap';
import ReactLeafletKml from 'react-leaflet-kml';
// @ts-ignore
import { kml as KMLFormat } from '@tmcw/togeojson';

type PageProps = {
  data: Kml;
};

const RESOURCE = 'kmls.update';

const Edit = () => {
  const { data: KmlProp } = usePage<Page<PageProps & DefaultPageProps>>().props;
  const { data, setData, post, processing, errors, patch, transform } = useForm<Kml & { _method: string }>({
    route_name: KmlProp.route_name,
    id_kml: KmlProp.id_kml,
    active: KmlProp.active,
    file: KmlProp.url,
    id: KmlProp.id,
    _method: 'PUT',
  });
  const { enqueueSnackbar } = useSnackbar();

  const [formatKml, setFormatKml] = useState<any>(null);

  useEffect(() => {
    setFormatKml(null);
    if (data.file) {
      let type = typeof data.file;
      if (type === 'string') {
        fetch(data.file)
          .then((res) => res.text())
          .then((kmlText) => {
            const parser = new DOMParser();
            const kml = parser.parseFromString(kmlText, 'text/xml');
            if (kml.documentElement.nodeName == 'kml') {
              for (const item of kml.getElementsByTagName('Placemark') as any) {
                let placeMarkName = item.getElementsByTagName('name')[0].childNodes[0].nodeValue.trim();
                setData((prev: any) => {
                  return {
                    ...prev,
                    route_name: placeMarkName,
                    geojson: KMLFormat(kml),
                  };
                });
              }
            }
            setFormatKml(kml);
          });
      } else if (type === 'object') {
        const reader = new FileReader();
        reader.readAsText(data.file);
        reader.onload = () => {
          let data: any = reader.result;
          const parser = new DOMParser();
          const kml = parser.parseFromString(data, 'text/xml');
          if (kml.documentElement.nodeName == 'kml') {
            for (const item of kml.getElementsByTagName('Placemark') as any) {
              let placeMarkName = item.getElementsByTagName('name')[0].childNodes[0].nodeValue.trim();
              setData((prev: any) => {
                return {
                  ...prev,
                  route_name: placeMarkName,
                  geojson: KMLFormat(kml),
                };
              });
            }
          }
          setFormatKml(kml);
        };
        reader.onerror = () => {
          reader.abort();
        };
      }
    }
  }, [data.file]);

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    transform((prev: any) => {
      return {
        ...prev,
        _method: 'PUT',
      };
    });
    post(route(RESOURCE, [data.id]), {
      onSuccess: () => {
        enqueueSnackbar('KML actualizado exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <Paper
        onSubmit={submit}
        component="form"
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <Dropzone
            getCurrentImage={(file: any) => {
              setData('file', file[0]);
            }}
            file={data.file ? data.file : null}
            title="seleciona el archivo del kml"
            error={!!errors.file}
            helperText={errors.file ? errors.file : ''}
          />

          {data.file && formatKml ? (
            <LeaftletMap>
              <ReactLeafletKml kml={formatKml} />
            </LeaftletMap>
          ) : null}
          <TextField
            label="Nombre de la ruta"
            value={data.route_name}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('route_name', e.target.value)}
            error={!!errors.route_name}
            helperText={errors.route_name ? errors.route_name : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            label="ID ruta"
            value={data.id_kml}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('id_kml', e.target.value)}
            error={!!errors.id_kml}
            helperText={errors.id_kml ? errors.id_kml : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />

          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    checked={data.active}
                    onChange={() => {
                      setData('active', !data.active);
                    }}
                  />
                }
                label={data.active ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
          <Box
            sx={{
              marginTop: 2,
              textAlign: 'center',
            }}
          >
            <Button type="submit" disabled={processing} variant="contained" color="primary">
              {processing ? <CircularProgress size={20} /> : 'Actualizar'}
            </Button>
          </Box>
        </Container>
      </Paper>
    </div>
  );
};

Edit.layout = (page: any) => <Layout children={page} title="Editar KML" />;

export default Edit;
