import React, { useEffect, useState } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';
import { Kml, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';
import Dropzone from '../../common/components/DropzoneMasiveFile';
import LeaftletMap from '../../common/components/LeaftletMap';
import ReactLeafletKml from 'react-leaflet-kml';
// @ts-ignore
import { kml as KMLFormat } from '@tmcw/togeojson';

const RESOURCE = 'kmls.store.masive';

const Create = () => {
  const { data, setData, post, processing, errors, reset } = useForm<any>({
    files: [],
    filesWithKml: [],
  });
  const [ready, setReady] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (data.files) {
      data.files.forEach((f: any) => {
        let clone = data.filesWithKml;
        let file = f;
        const reader = new FileReader();
        reader.readAsText(file);
        reader.onload = () => {
          let data: any = reader.result;
          const parser = new DOMParser();
          const kml = parser.parseFromString(data, 'text/xml');
          if (kml.documentElement.nodeName == 'kml') {
            for (const item of kml.getElementsByTagName('Placemark') as any) {
              let placeMarkName = item.getElementsByTagName('name')[0].childNodes[0].nodeValue.trim();

              let val = {
                geojson: JSON.stringify(KMLFormat(kml)),
                file: file,
                name: placeMarkName ? placeMarkName : '',
              };

              clone.push(val);

              setData((prev: any) => {
                return {
                  ...prev,
                  filesWithKml: clone,
                };
              });
              setReady(true);
            }
          }
        };

        reader.onerror = () => {
          reader.abort();
        };
      });
    }
  }, [data.files]);

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    post(route(RESOURCE), {
      onSuccess: () => {
        enqueueSnackbar('KML creado exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <Paper
        onSubmit={submit}
        component="form"
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <Dropzone
            getCurrentImage={(files: any) => {
              setData('files', files);
            }}
            files={data.files ? data.files : null}
            title="seleciona el archivo del kml"
            error={!!errors.file}
            helperText={errors.file ? errors.file : ''}
          />

          {data.files.map((item: any, i: number) => (
            <div key={i}>{JSON.stringify(item)}</div>
          ))}

          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    checked={data.active}
                    onChange={() => {
                      setData('active', !data.active);
                    }}
                  />
                }
                label={data.active ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
          <Box
            sx={{
              marginTop: 2,
              textAlign: 'center',
            }}
          >
            <Button type="submit" disabled={!ready} variant="contained" color="primary">
              {processing ? <CircularProgress size={20} /> : 'Crear'}
            </Button>
          </Box>
        </Container>
      </Paper>
    </div>
  );
};

Create.layout = (page: any) => <Layout children={page} title="Crear KML" />;

export default Create;

// export default Create;
