import { Inertia, Page } from '@inertiajs/inertia';
import { usePage } from '@inertiajs/inertia-react';
import React from 'react';
import { CellProps, Column } from 'react-table';
import { Kml, Customer, DefaultPageProps } from 'resources/js/types';
import Layout from '../../layouts/Layout';
import {
  Edit as EditIcon,
  Visibility as VisibilityIcon,
  Add as AddIcon,
  ToggleOff as MuiToggleOff,
  ToggleOn as MuiToggleOn,
  Refresh as RefreshIcon,
} from '@mui/icons-material';
import { TableContainer, Paper, makeStyles, IconButton, Tooltip, Button, AccordionSummaryTypeMap } from '@mui/material';
import SelectColumnFilter from '@common/components/data-table/filters/SelectColumnFilter';
// @ts-ignore
import route from 'ziggy';
import DataTable from '@common/components/data-table';
import { styled } from '@mui/material/styles';
import DateColumnFilter from '@common/components/data-table/filters/DateColumnFilter';
import moment from 'moment';

const ACTIVE_FILTER_OPTIONS = [
  { label: 'Todos', value: '' },
  { label: 'Activo', value: '1' },
  { label: 'Inactivo', value: '0' },
];

const RESOURCE = 'kmls';

const ToggleOn = styled(MuiToggleOn)(({ theme }) => ({
  color: 'green',
  fontSize: '2.5rem',
}));

const ToggleOff = styled(MuiToggleOn)(({ theme }) => ({
  fontSize: '2.5rem',
}));

type PageProps = {
  data: {
    data: Array<Kml>;
    current_page: number;
    total: number;
    per_page: number;
  };
};

const Index = ({}) => {
  const { data: pagination } = usePage<Page<PageProps & DefaultPageProps>>().props;

  const data = React.useMemo(() => pagination.data, [pagination.data]);

  const columns: Array<Column<Kml>> = React.useMemo(
    () => [
      // {
      //   Header: 'ID ruta',
      //   accessor: 'id_kml',
      //   width: 120,
      // },
      {
        Header: 'Nombre de la ruta',
        accessor: 'route_name',
      },
      {
        Header: 'url',
        accessor: 'url',
        disableFilters: true,
      },
      {
        Header: 'Actualizado',
        accessor: 'updated_at',
        Cell: ({ value }: { value: string }) => {
          if (value) {
            return moment(value).format('YYYY-MM-DD');
          }
          return '';
        },
        Filter: DateColumnFilter,
      },
      {
        Header: 'Activo',
        accessor: 'active',
        Cell: ({ value }: { value: boolean }) => {
          if (value) {
            return <ToggleOn />;
          }

          return (
            <ToggleOff
              sx={{
                fontSize: '2.5rem',
              }}
            />
          );
        },
        disableSortBy: true,
        Filter: (props) => <SelectColumnFilter column={props.column} options={ACTIVE_FILTER_OPTIONS} />,
      },
    ],
    []
  );

  const onChangeTable = ({
    page,
    perPage,
    // @ts-ignore
    sort,
    filters,
  }: {
    page: number;
    perPage: number;
    sort?: string;
    filters?: { [key: string]: any };
  }) => {
    Inertia.get(
      route(`${RESOURCE}.index`),
      {
        page,
        per_page: perPage,
        // @ts-ignore
        sort,
        // @ts-ignore
        filter: filters,
      },
      {
        preserveState: true,
        preserveScroll: true,
        only: ['data'],
      }
    );
  };

  const additionalColumns: Array<Column<Customer>> = React.useMemo(
    () => [
      {
        id: 'actions',
        Header: 'Acciones',
        width: 100,
        Cell: ({ row }: React.PropsWithChildren<CellProps<Customer>>) => (
          <>
            <Tooltip title="Ver detalles">
              <IconButton
                sx={{
                  padding: 0.5,
                }}
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.show`, [row.original.id]));
                }}
              >
                <VisibilityIcon
                  sx={{
                    fontSize: '1.3rem',
                  }}
                />
              </IconButton>
            </Tooltip>
            <Tooltip title="Editar">
              <IconButton
                sx={{
                  padding: 0.5,
                }}
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.edit`, [row.original.id]));
                }}
              >
                <EditIcon
                  sx={{
                    fontSize: '1.3rem',
                  }}
                />
              </IconButton>
            </Tooltip>
          </>
        ),
      },
    ],
    []
  );

  return (
    <>
      <TableContainer component={Paper}>
        <DataTable
          columns={columns}
          data={data}
          currentPage={pagination.current_page}
          currentPerPage={pagination.per_page}
          totalCount={pagination.total}
          onChange={onChangeTable}
          additionalColumns={additionalColumns}
          renderToolbar={
            <>
              {/* <Button
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.create.masive`));
                }}
                sx={{
                  marginRight: 1,
                }}
                variant="outlined"
                color="primary"
                size="small"
                startIcon={<AddIcon />}
              >
                Masivamente
              </Button> */}
              <Button
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.create`));
                }}
                sx={{
                  marginRight: 1,
                }}
                variant="outlined"
                color="primary"
                size="small"
                startIcon={<AddIcon />}
              >
                Nuevo
              </Button>
              <Tooltip title="Recargar resultados">
                <IconButton
                  sx={{
                    padding: 0.5,
                  }}
                  onClick={() => {
                    Inertia.reload();
                  }}
                >
                  <RefreshIcon />
                </IconButton>
              </Tooltip>
            </>
          }
        />
      </TableContainer>
    </>
  );
};

Index.layout = (page: any) => <Layout children={page} title="Kmls" />;

export default Index;
