import React, { useEffect, useState } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';
import { Kml, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';
import Dropzone from '../../common/components/DropzoneFile';
import LeaftletMap from '../../common/components/LeaftletMap';
import ReactLeafletKml from 'react-leaflet-kml';
// @ts-ignore
import { kml as KMLFormat } from '@tmcw/togeojson';

const RESOURCE = 'kmls.store';

const Create = () => {
  const { data, setData, post, processing, errors, reset } = useForm<Kml>({
    route_name: '',
    active: true,
    file: null,
    id_kml: '',
    geojson: null,
  });
  const { enqueueSnackbar } = useSnackbar();

  const [formatKml, setFormatKml] = useState<any>(null);

  useEffect(() => {
    if (data.file) {
      const reader = new FileReader();
      let id = data.file.path.split('.')[0];
      reader.readAsText(data.file);
      reader.onload = () => {
        let data: any = reader.result;
        const parser = new DOMParser();
        const kml = parser.parseFromString(data, 'text/xml');
        if (kml.documentElement.nodeName == 'kml') {
          for (const item of kml.getElementsByTagName('Placemark') as any) {
            let placeMarkName = item.getElementsByTagName('name')[0].childNodes[0].nodeValue.trim();
            console.log(KMLFormat(kml));

            setData((prev: any) => {
              return {
                ...prev,
                route_name: placeMarkName,
                geojson: JSON.stringify(KMLFormat(kml)),
                id_kml: id,
              };
            });
          }
        }

        setFormatKml(kml);
      };
      reader.onerror = () => {
        reader.abort();
      };
    }
  }, [data.file]);

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    post(route(RESOURCE), {
      onSuccess: () => {
        enqueueSnackbar('KML creado exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <Paper
        onSubmit={submit}
        component="form"
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <Dropzone
            getCurrentImage={(file: any) => {
              setData('file', file[0]);
            }}
            file={data.file ? data.file : null}
            title="seleciona el archivo del kml"
            error={!!errors.file}
            helperText={errors.file ? errors.file : ''}
          />
          {data.file && formatKml ? (
            <LeaftletMap>
              <ReactLeafletKml kml={formatKml} />
            </LeaftletMap>
          ) : null}

          <TextField
            label="Nombre de la ruta"
            value={data.route_name}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('route_name', e.target.value)}
            error={!!errors.route_name}
            helperText={errors.route_name ? errors.route_name : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            disabled
            label="ID ruta"
            value={data.id_kml}
            fullWidth
            variant="outlined"
            margin="dense"
            // onChange={(e) => setData('id_kml', e.target.value)}
            error={!!errors.id_kml}
            helperText={errors.id_kml ? errors.id_kml : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />

          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    checked={data.active}
                    onChange={() => {
                      setData('active', !data.active);
                    }}
                  />
                }
                label={data.active ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
          <Box
            sx={{
              marginTop: 2,
              textAlign: 'center',
            }}
          >
            <Button type="submit" disabled={processing} variant="contained" color="primary">
              {processing ? <CircularProgress size={20} /> : 'Crear'}
            </Button>
          </Box>
        </Container>
      </Paper>
    </div>
  );
};

Create.layout = (page: any) => <Layout children={page} title="Crear KML" />;

export default Create;

// export default Create;
