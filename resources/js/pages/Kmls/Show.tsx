import React, { useEffect, useState } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';
import { Kml, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import ReactLeafletKml from 'react-leaflet-kml';
import 'leaflet/dist/leaflet.css';

type PageProps = {
  data: Kml;
};

const Show = () => {
  const { data } = usePage<Page<PageProps & DefaultPageProps>>().props;

  const [formatKml, setFormatKml] = useState<any>(null);

  useEffect(() => {
    if (data.url) {
      fetch(data.url)
        .then((res) => res.text())
        .then((kmlText) => {
          const parser = new DOMParser();
          const kml = parser.parseFromString(kmlText, 'text/xml');
          setFormatKml(kml);
        });
    }
  }, [data.url]);

  return (
    <div>
      <Paper
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          {formatKml ? (
            <FormControl fullWidth variant="outlined" margin="dense">
              <InputLabel
                shrink
                style={{
                  marginBottom: 100,
                }}
              >
                Ruta de archivo KML
              </InputLabel>
              <Box
                sx={{
                  marginTop: 1,
                }}
              >
                <MapContainer
                  center={[25.7002764, -100.2503261]}
                  zoom={10}
                  scrollWheelZoom={false}
                  style={{
                    width: '100%',
                    height: 400,
                  }}
                >
                  <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                  />
                  <ReactLeafletKml kml={formatKml} />
                </MapContainer>
              </Box>
            </FormControl>
          ) : null}

          <TextField
            label="Nombre de la ruta"
            value={data.route_name}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            label="ID ruta"
            value={data.id_kml}
            fullWidth
            variant="outlined"
            margin="dense"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={<Switch color="primary" checked={data.active} />}
                label={data.active ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
        </Container>
      </Paper>
    </div>
  );
};

Show.layout = (page: any) => <Layout children={page} title="Detalles de KML" />;

export default Show;
