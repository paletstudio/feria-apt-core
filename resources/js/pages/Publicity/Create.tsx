import React, { ChangeEvent, useEffect, useState } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
  Autocomplete,
} from '@mui/material';
import { Publicity, DefaultPageProps, Zone } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Inertia, Page } from '@inertiajs/inertia';
import { CKEditor } from '@ckeditor/ckeditor5-react';
// @ts-ignore
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';
import { CalendarToday as CalendarIcon } from '@mui/icons-material';
import moment from 'moment';
import Dropzone from '../../common/components/DropzoneImage';
import MobileDatePicker from '@mui/lab/MobileDatePicker';

const RESOURCE = 'publicity.store';

type PageProps = {
  zones: any;
};

const Create = () => {
  const { zones } = usePage<Page<PageProps & DefaultPageProps>>().props;
  const { data, setData, post, processing, errors, reset } = useForm<Publicity>({
    descripcion: '',
    fecha_fin: null,
    fecha_inicio: null,
    banner: null,
    activo: true,
    zona_id: null,
    link: '',
  });
  const { enqueueSnackbar } = useSnackbar();
  const [currentZone, setCurrentZone] = useState<any>(null);

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    post(route(RESOURCE), {
      onSuccess: () => {
        enqueueSnackbar('Publicidad creada exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <Paper
        onSubmit={submit}
        component="form"
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <TextField
            label="Descripción"
            value={data.descripcion}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('descripcion', e.target.value)}
            error={!!errors.descripcion}
            helperText={errors.descripcion ? errors.descripcion : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />

          <Autocomplete
            fullWidth
            getOptionLabel={(option: Zone) => option.nombre}
            options={zones || []}
            onChange={(e: ChangeEvent<{}>, newValue: Zone | null) => {
              setCurrentZone(newValue);
              if (newValue && newValue.id) {
                setData('zona_id', newValue.id);
              } else {
                setData('zona_id', 0);
              }
            }}
            value={currentZone}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Zona"
                variant="outlined"
                margin="dense"
                placeholder="Selecione la zona"
                autoComplete="off"
                InputLabelProps={{
                  shrink: true,
                }}
                error={!!errors.zona_id}
                helperText={errors.zona_id ? errors.zona_id : ''}
              />
            )}
          />

          <MobileDatePicker
            label="Fecha inicio"
            clearText="Limpiar"
            cancelText="Cancelar"
            clearable
            value={data.fecha_inicio}
            onChange={(date: any) => {
              // setData('fecha_inicio', date ? date.format('YYYY-MM-DD') : null);
            }}
            onAccept={(date: any) => {
              setData('fecha_inicio', date ? date.format('YYYY-MM-DD') : null);
            }}
            inputFormat="YYYY-MM-DD"
            toolbarTitle="Elige una fecha"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <CalendarIcon style={{ fontSize: 20 }} />
                </InputAdornment>
              ),
            }}
            renderInput={(params) => (
              <TextField
                InputLabelProps={{
                  shrink: true,
                }}
                fullWidth
                sx={{
                  '& p': {
                    color: '#D32F2F',
                  },
                }}
                error={!!errors.fecha_inicio}
                helperText={errors.fecha_inicio ? errors.fecha_inicio : ''}
                {...params}
              />
            )}
          />

          <MobileDatePicker
            label="Fecha fin"
            clearText="Limpiar"
            cancelText="Cancelar"
            clearable
            value={data.fecha_fin}
            onChange={(date: any) => {
              // setData('fecha_inicio', date ? date.format('YYYY-MM-DD') : null);
            }}
            onAccept={(date: any) => {
              setData('fecha_fin', date ? date.format('YYYY-MM-DD') : null);
            }}
            inputFormat="YYYY-MM-DD"
            toolbarTitle="Elige una fecha"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <CalendarIcon style={{ fontSize: 20 }} />
                </InputAdornment>
              ),
            }}
            renderInput={(params) => (
              <TextField
                InputLabelProps={{
                  shrink: true,
                }}
                fullWidth
                sx={{
                  '& p': {
                    color: '#D32F2F',
                  },
                }}
                error={!!errors.fecha_fin}
                helperText={errors.fecha_fin ? errors.fecha_fin : ''}
                {...params}
              />
            )}
          />

          <TextField
            label="Enlace"
            value={data.link}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('link', e.target.value)}
            error={!!errors.link}
            helperText={errors.link ? errors.link : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />

          <Dropzone
            getCurrentImage={(image: any) => {
              setData('banner', image[0]);
            }}
            file={data.banner ? data.banner : null}
            title="seleciona la imagen de la publicidad"
            error={!!errors.banner}
            helperText={errors.banner ? errors.banner : ''}
          />

          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    checked={data.activo}
                    onChange={() => {
                      setData('activo', !data.activo);
                    }}
                  />
                }
                label={data.activo ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
          <Box
            sx={{
              marginTop: 2,
              textAlign: 'center',
            }}
          >
            <Button type="submit" disabled={processing} variant="contained" color="primary">
              {processing ? <CircularProgress size={20} /> : 'Crear'}
            </Button>
          </Box>
        </Container>
      </Paper>
    </div>
  );
};

Create.layout = (page: any) => <Layout children={page} title="Crear publicidad" />;

export default Create;

// export default Create;
