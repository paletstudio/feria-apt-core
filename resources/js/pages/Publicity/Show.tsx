import React, { ChangeEvent, useEffect, useState } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
  Autocomplete,
} from '@mui/material';
import { Publicity, DefaultPageProps, Zone } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Inertia, Page } from '@inertiajs/inertia';
import { CKEditor } from '@ckeditor/ckeditor5-react';
// @ts-ignore
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';
import { CalendarToday as CalendarIcon } from '@mui/icons-material';
import moment from 'moment';
import Dropzone from '../../common/components/DropzoneImage';
import MobileDatePicker from '@mui/lab/MobileDatePicker';

const RESOURCE = 'publicity.store';

type PageProps = {
  publicity: Publicity;
  zones: Zone[];
};

const Show = () => {
  const { publicity, zones } = usePage<Page<PageProps & DefaultPageProps>>().props;
  const { data, setData, post, processing, errors, reset } = useForm<Publicity>({
    descripcion: publicity.descripcion,
    fecha_fin: publicity.fecha_fin,
    fecha_inicio: publicity.fecha_fin,
    banner: publicity.banner,
    activo: publicity.activo,
    zona_id: publicity.zona_id,
    link: publicity.link,
  });
  const { enqueueSnackbar } = useSnackbar();
  const [currentZone, setCurrentZone] = useState<any>(publicity?.zone || null);

  return (
    <div>
      <Paper
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <TextField
            label="Descripción"
            value={data.descripcion}
            fullWidth
            variant="outlined"
            margin="dense"
            // onChange={(e) => setData('descripcion', e.target.value)}
            error={!!errors.descripcion}
            helperText={errors.descripcion ? errors.descripcion : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />

          <Autocomplete
            disabled
            fullWidth
            getOptionLabel={(option: Zone) => option.nombre}
            options={zones || []}
            onChange={(e: ChangeEvent<{}>, newValue: Zone | null) => {}}
            value={currentZone}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Zona"
                variant="outlined"
                margin="dense"
                placeholder="Selecione la zona"
                autoComplete="off"
                InputLabelProps={{
                  shrink: true,
                }}
                error={!!errors.zona_id}
                helperText={errors.zona_id ? errors.zona_id : ''}
              />
            )}
          />

          <MobileDatePicker
            disabled
            label="Fecha inicio"
            clearText="Limpiar"
            cancelText="Cancelar"
            clearable
            value={data.fecha_inicio}
            onChange={(date: any) => {
              // setData('fecha_inicio', date ? date.format('YYYY-MM-DD') : null);
            }}
            onAccept={(date: any) => {
              // setData('fecha_inicio', date ? date.format('YYYY-MM-DD') : null);
            }}
            inputFormat="YYYY-MM-DD"
            toolbarTitle="Elige una fecha"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <CalendarIcon style={{ fontSize: 20 }} />
                </InputAdornment>
              ),
            }}
            renderInput={(params) => (
              <TextField
                InputLabelProps={{
                  shrink: true,
                }}
                fullWidth
                sx={{
                  '& p': {
                    color: '#D32F2F',
                  },
                }}
                error={!!errors.fecha_inicio}
                helperText={errors.fecha_inicio ? errors.fecha_inicio : ''}
                {...params}
              />
            )}
          />

          <MobileDatePicker
            disabled
            label="Fecha fin"
            clearText="Limpiar"
            cancelText="Cancelar"
            clearable
            value={data.fecha_fin}
            onChange={(date: any) => {
              // setData('fecha_inicio', date ? date.format('YYYY-MM-DD') : null);
            }}
            onAccept={(date: any) => {
              // setData('fecha_fin', date ? date.format('YYYY-MM-DD') : null);
            }}
            inputFormat="YYYY-MM-DD"
            toolbarTitle="Elige una fecha"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <CalendarIcon style={{ fontSize: 20 }} />
                </InputAdornment>
              ),
            }}
            renderInput={(params) => (
              <TextField
                InputLabelProps={{
                  shrink: true,
                }}
                fullWidth
                sx={{
                  '& p': {
                    color: '#D32F2F',
                  },
                }}
                error={!!errors.fecha_fin}
                helperText={errors.fecha_fin ? errors.fecha_fin : ''}
                {...params}
              />
            )}
          />

          <TextField
            label="Enlace"
            value={data.link}
            fullWidth
            variant="outlined"
            margin="dense"
            // onChange={(e) => setData('link', e.target.value)}
            error={!!errors.link}
            helperText={errors.link ? errors.link : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />

          <Dropzone
            disabled
            getCurrentImage={(image: any) => {
              // setData('banner', image[0]);
            }}
            file={data.banner ? data.banner : null}
            title="seleciona la imagen de la publicidad"
          />

          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    checked={data.activo}
                    onChange={() => {
                      // setData('activo', !data.activo);
                    }}
                  />
                }
                label={data.activo ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
        </Container>
      </Paper>
    </div>
  );
};

Show.layout = (page: any) => <Layout children={page} title="Detalles de publicidad" />;

export default Show;
