import React, { ChangeEvent, useEffect, useState } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
  Autocomplete,
} from '@mui/material';
import { Rate, DefaultPageProps, Zone } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Inertia, Page } from '@inertiajs/inertia';
import { CKEditor } from '@ckeditor/ckeditor5-react';
// @ts-ignore
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';
import { CalendarToday as CalendarIcon } from '@mui/icons-material';
import moment from 'moment';
import Dropzone from '../../common/components/DropzoneImage';
import MobileDatePicker from '@mui/lab/MobileDatePicker';
import NumberFormat from '@common/components/NumberFormat';

const RESOURCE = 'rates.store';

type PageProps = {
  zones: any;
};

const Create = () => {
  const { zones } = usePage<Page<PageProps & DefaultPageProps>>().props;
  const { data, setData, post, processing, errors, reset } = useForm<Rate>({
    nombre: '',
    tipo: null,
    precio: null,
    fecha_fin: '',
    fecha_tarifa: '',
    ac: 0,
    activo: true,
  });
  const { enqueueSnackbar } = useSnackbar();
  const [currentType, setCurrentType] = useState<any>(null);

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    post(route(RESOURCE), {
      onSuccess: () => {
        enqueueSnackbar('Tarifa creada exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <Paper
        onSubmit={submit}
        component="form"
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <TextField
            label="Nombre"
            value={data.nombre}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('nombre', e.target.value)}
            error={!!errors.nombre}
            helperText={errors.nombre ? errors.nombre : ''}
            InputLabelProps={{
              shrink: true,
            }}
          />

          <Autocomplete
            isOptionEqualToValue={(option: any, value: any) => option.value === value.value}
            fullWidth
            getOptionLabel={(option: any) => option.label}
            options={[
              { label: 'Preferente', value: 2 },
              { label: 'Ordinario', value: 1 },
            ]}
            onChange={(e: ChangeEvent<{}>, newValue: any) => {
              setCurrentType(newValue);
              if (newValue && newValue.value) {
                setData('tipo', newValue.value);
              } else {
                setData('tipo', null);
              }
            }}
            value={currentType}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Tipo"
                variant="outlined"
                margin="dense"
                placeholder="Selecione el tipo de tarifa"
                autoComplete="off"
                InputLabelProps={{
                  shrink: true,
                }}
                error={!!errors.tipo}
                helperText={errors.tipo ? errors.tipo : ''}
              />
            )}
          />
          <MobileDatePicker
            label="Fecha de inicio"
            clearText="Limpiar"
            cancelText="Cancelar"
            clearable
            value={data.fecha_tarifa}
            onChange={(date: any) => {
              // setData('fecha_inicio', date ? date.format('YYYY-MM-DD') : null);
            }}
            onAccept={(date: any) => {
              setData('fecha_tarifa', date ? date.format('YYYY-MM-DD') : null);
            }}
            inputFormat="YYYY-MM-DD"
            toolbarTitle="Elige una fecha"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <CalendarIcon style={{ fontSize: 20 }} />
                </InputAdornment>
              ),
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                InputLabelProps={{
                  shrink: true,
                }}
                fullWidth
                sx={{
                  '& p': {
                    color: '#D32F2F',
                  },
                }}
                error={!!errors.fecha_tarifa}
                helperText={errors.fecha_tarifa ? errors.fecha_tarifa : ''}
              />
            )}
          />
          <MobileDatePicker
            label="Fecha de fin"
            clearText="Limpiar"
            cancelText="Cancelar"
            clearable
            value={data.fecha_fin}
            onChange={(date: any) => {
              // setData('fecha_inicio', date ? date.format('YYYY-MM-DD') : null);
            }}
            onAccept={(date: any) => {
              setData('fecha_fin', date ? date.format('YYYY-MM-DD') : null);
            }}
            inputFormat="YYYY-MM-DD"
            toolbarTitle="Elige una fecha"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <CalendarIcon style={{ fontSize: 20 }} />
                </InputAdornment>
              ),
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                InputLabelProps={{
                  shrink: true,
                }}
                fullWidth
                sx={{
                  '& p': {
                    color: '#D32F2F',
                  },
                }}
                error={!!errors.fecha_fin}
                helperText={errors.fecha_fin ? errors.fecha_fin : ''}
              />
            )}
          />

          <TextField
            label="Precio"
            value={data.precio}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('precio', +e.target.value)}
            error={!!errors.precio}
            helperText={errors.precio ? errors.precio : ''}
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              inputComponent: NumberFormat as any,
            }}
          />

          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Aire Acondicionado</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    checked={data.ac === 1 ? true : false}
                    onChange={() => {
                      setData('ac', data.ac === 1 ? 0 : 1);
                    }}
                  />
                }
                label={data.ac === 1 ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    checked={data.activo}
                    onChange={() => {
                      setData('activo', !data.activo);
                    }}
                  />
                }
                label={data.activo ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
          <Box
            sx={{
              marginTop: 2,
              textAlign: 'center',
            }}
          >
            <Button type="submit" disabled={processing} variant="contained" color="primary">
              {processing ? <CircularProgress size={20} /> : 'Crear'}
            </Button>
          </Box>
        </Container>
      </Paper>
    </div>
  );
};

Create.layout = (page: any) => <Layout children={page} title="Crear tarifa" />;

export default Create;
