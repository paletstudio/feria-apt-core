import { Inertia, Page } from '@inertiajs/inertia';
import { usePage } from '@inertiajs/inertia-react';
import React from 'react';
import { CellProps, Column } from 'react-table';
import { Advice, DefaultPageProps, Publicity, Rate } from 'resources/js/types';
import Layout from '../../layouts/Layout';
import {
  Edit as EditIcon,
  Visibility as VisibilityIcon,
  Add as AddIcon,
  ToggleOff as MuiToggleOff,
  ToggleOn as MuiToggleOn,
  Refresh as RefreshIcon,
} from '@mui/icons-material';
import { TableContainer, Paper, makeStyles, IconButton, Tooltip, Button, AccordionSummaryTypeMap } from '@mui/material';
import SelectColumnFilter from '@common/components/data-table/filters/SelectColumnFilter';
// @ts-ignore
import route from 'ziggy';
import DataTable from '@common/components/data-table';
import { styled } from '@mui/material/styles';
import DateColumnFilter from '@common/components/data-table/filters/DateColumnFilter';
import moment from 'moment';

const ACTIVE_FILTER_OPTIONS = [
  { label: 'Todos', value: '' },
  { label: 'Activo', value: '1' },
  { label: 'Inactivo', value: '0' },
];

const TYPE_FILTER_OPTIONS = [
  { label: 'Todos', value: '' },
  { label: 'Preferente', value: 2 },
  { label: 'Ordinario', value: 1 },
];

const AC_FILTER_OPTIONS = [
  { label: 'Todos', value: '' },
  { label: 'Si', value: '1' },
  { label: 'No', value: '0' },
];

const RESOURCE = 'rates';

const ToggleOn = styled(MuiToggleOn)(({ theme }) => ({
  color: 'green',
  fontSize: '2.5rem',
}));

const ToggleOff = styled(MuiToggleOn)(({ theme }) => ({
  fontSize: '2.5rem',
}));

type PageProps = {
  data: {
    data: Array<Rate>;
    current_page: number;
    total: number;
    per_page: number;
  };
};

const Index = ({}) => {
  const { data: pagination } = usePage<Page<PageProps & DefaultPageProps>>().props;

  const data = React.useMemo(() => pagination.data, [pagination.data]);

  const columns: Array<Column<Rate>> = React.useMemo(
    () => [
      // {
      //   Header: 'ID',
      //   accessor: 'id',
      //   width: 120,
      //   // defaultCanSort: true,
      // },
      {
        Header: 'Nombre',
        accessor: 'nombre',
        // defaultCanSort: true,
      },
      {
        Header: 'Tipo',
        accessor: 'tipo',
        Cell: ({ value }: { value: number }) => {
          return value === 1 ? 'Ordinario' : 'Preferente';
        },
        Filter: (props) => <SelectColumnFilter column={props.column} options={TYPE_FILTER_OPTIONS} />,
      },
      {
        Header: 'Aire Acondicionado',
        accessor: 'ac',
        Cell: ({ value }: { value: boolean }) => {
          return value ? 'Si' : 'No';
        },
        Filter: (props) => <SelectColumnFilter column={props.column} options={AC_FILTER_OPTIONS} />,
      },
      {
        Header: 'Precio',
        accessor: 'precio',
        Cell: ({ value }: { value: string }) => {
          if (value) {
            return `MX$${value}`;
          }
          return '';
        },
        // defaultCanSort: true,
      },
      {
        Header: 'Fecha de inicio',
        accessor: 'fecha_tarifa',
        Cell: ({ value }: { value: string }) => {
          if (value) {
            return moment(value).format('YYYY-MM-DD');
          }
          return '';
        },
        Filter: DateColumnFilter,
      },
      {
        Header: 'Fecha de fin',
        accessor: 'fecha_fin',
        Cell: ({ value }: { value: string }) => {
          if (value) {
            return moment(value).format('YYYY-MM-DD');
          }
          return '';
        },
        Filter: DateColumnFilter,
      },
      {
        Header: 'Activo',
        accessor: 'activo',
        Cell: ({ value }: { value: boolean }) => {
          if (value) {
            return <ToggleOn />;
          }

          return (
            <ToggleOff
              sx={{
                fontSize: '2.5rem',
              }}
            />
          );
        },
        disableSortBy: true,
        Filter: (props) => <SelectColumnFilter column={props.column} options={ACTIVE_FILTER_OPTIONS} />,
      },
    ],
    []
  );

  const onChangeTable = ({
    page,
    perPage,
    // @ts-ignore
    sort,
    filters,
  }: {
    page: number;
    perPage: number;
    sort?: string;
    filters?: { [key: string]: any };
  }) => {
    Inertia.get(
      route(`${RESOURCE}.index`),
      {
        page,
        per_page: perPage,
        // @ts-ignore
        sort,
        // @ts-ignore
        filter: filters,
      },
      {
        preserveState: true,
        preserveScroll: true,
        only: ['data'],
      }
    );
  };

  const additionalColumns: Array<Column<Rate>> = React.useMemo(
    () => [
      {
        id: 'actions',
        Header: 'Acciones',
        width: 100,
        Cell: ({ row }: React.PropsWithChildren<CellProps<Rate>>) => (
          <>
            <Tooltip title="Ver detalles">
              <IconButton
                sx={{
                  padding: 0.5,
                }}
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.show`, [row.original.id]));
                }}
              >
                <VisibilityIcon
                  sx={{
                    fontSize: '1.3rem',
                  }}
                />
              </IconButton>
            </Tooltip>
            <Tooltip title="Editar">
              <IconButton
                sx={{
                  padding: 0.5,
                }}
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.edit`, [row.original.id]));
                }}
              >
                <EditIcon
                  sx={{
                    fontSize: '1.3rem',
                  }}
                />
              </IconButton>
            </Tooltip>
          </>
        ),
      },
    ],
    []
  );

  return (
    <>
      <TableContainer component={Paper}>
        <DataTable
          columns={columns}
          data={data}
          currentPage={pagination.current_page}
          currentPerPage={pagination.per_page}
          totalCount={pagination.total}
          onChange={onChangeTable}
          additionalColumns={additionalColumns}
          renderToolbar={
            <>
              <Button
                onClick={() => {
                  Inertia.visit(route(`${RESOURCE}.create`));
                }}
                sx={{
                  marginRight: 1,
                }}
                variant="outlined"
                color="primary"
                size="small"
                startIcon={<AddIcon />}
              >
                Nuevo
              </Button>
              <Tooltip title="Recargar resultados">
                <IconButton
                  sx={{
                    padding: 0.5,
                  }}
                  onClick={() => {
                    Inertia.reload();
                  }}
                >
                  <RefreshIcon />
                </IconButton>
              </Tooltip>
            </>
          }
        />
      </TableContainer>
    </>
  );
};

Index.layout = (page: any) => <Layout children={page} title="Tarifas" />;

export default Index;
