import React, { useEffect, useState } from 'react';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import InputAdornment from '@mui/material/InputAdornment';
import PersonIcon from '@mui/icons-material/Person';
import LockIcon from '@mui/icons-material/Lock';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useForm } from '@inertiajs/inertia-react';
import LoadingButton from '@mui/lab/LoadingButton';
// @ts-ignore
import route from 'ziggy';

function Copyright(props: any) {
  return (
    <Typography colo variant="subtitle1" style={{ color: '#374767', fontWeight: 'bold' }} align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://www.enlacesinteligentes.com.mx/">
        EISA
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'} Todos los derechos reservados.
    </Typography>
  );
}

export default function SignInSide() {
  const { data, setData, post, processing, errors } = useForm({
    email: '',
    password: '',
    remember: false,
  });

  function handleSubmit(e: any) {
    e.preventDefault();
    post(route('login'), {
      replace: true,
    });
  }

  useEffect(() => {
    document.title = 'Tarjeta Feria | Sistema de avisos publicidad y tarifas';
  }, []);

  return (
    <Grid container component="main" sx={{ height: '100vh' }}>
      <Grid
        item
        xs={false}
        sm={4}
        md={7}
        sx={{
          backgroundImage: `url(${'images/login.png'})`,
          backgroundRepeat: 'no-repeat',
          backgroundColor: (t) => (t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900]),
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }}
      />
      <Grid
        item
        xs={12}
        sm={8}
        md={5}
        component={Paper}
        elevation={6}
        square
        sx={{
          backgroundColor: '#70cee6',
        }}
      >
        <Box
          sx={{
            my: 8,
            mx: 4,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <img
            src="/images/logo.png"
            style={{
              height: 200,
            }}
          />

          {/* <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <img src="/images/logo.png" />
            <LockOutlinedIcon />
          </Avatar> */}
          <Typography
            component="h1"
            variant="h4"
            sx={{
              mt: 2,
              color: '#374767',
            }}
          >
            Sistema de avisos, publicidad y tarifas
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ p: 2, mt: 5, backgroundColor: 'white', borderRadius: 1 }}
          >
            <TextField
              placeholder="Email"
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              onChange={(e) => {
                setData('email', e.target.value);
              }}
              value={data.email}
              error={!!errors.email}
              helperText={errors.email || ''}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PersonIcon />
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              type="password"
              placeholder="Contraseña"
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="password"
              onChange={(e) => {
                setData('password', e.target.value);
              }}
              value={data.password}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <LockIcon />
                  </InputAdornment>
                ),
              }}
            />
            <LoadingButton
              endIcon={<ArrowForwardIcon />}
              loading={processing}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              sx={{
                mt: 3,
                mx: 0,
                mb: 3,
              }}
              disabled={!data.email || !data.password}
            >
              Iniciar sesión
            </LoadingButton>
          </Box>
          <Copyright sx={{ mt: 5 }} />
        </Box>
      </Grid>
    </Grid>
  );
}
