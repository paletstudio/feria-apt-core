import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  FormHelperText,
} from '@mui/material';
import { DefaultPageProps, Version } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
import { useSnackbar } from 'notistack';
// @ts-ignore
import route from 'ziggy';
// import mobile from '../../assets/mobile.svg';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import IConMobile from '../../common/components/MobileSvg';

const RESOURCE = 'notifications.send';

const Index = () => {
  const { data, setData, post, processing, errors, reset } = useForm<any>({
    title: '',
    message: '',
  });
  const { enqueueSnackbar } = useSnackbar();

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    post(route(RESOURCE), {
      replace: true,
      onSuccess: () => {
        reset();
        enqueueSnackbar('Notificacion enviada exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <Paper
        onSubmit={submit}
        component="form"
        sx={{
          padding: 2,
        }}
      >
        <Grid container direction="row" justifyContent="center" alignItems="center">
          <Grid item>
            <Container maxWidth="sm">
              <IConMobile title={data.title} message={data.message} />
            </Container>
          </Grid>
          <Grid item xs>
            <Container>
              <TextField
                label="Titulo"
                value={data.title}
                fullWidth
                variant="outlined"
                margin="dense"
                onChange={(e) => setData('title', e.target.value)}
                error={!!errors.title}
                helperText={errors.title ? errors.title : ''}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{ maxLength: 20 }}
                placeholder="El titulo debe tener al menos 20 caracteres."
              />

              <TextField
                multiline={true}
                rows={5}
                label="Mensaje"
                value={data.message}
                fullWidth
                variant="outlined"
                margin="dense"
                onChange={(e) => {
                  setData('message', e.target.value);
                }}
                error={!!errors.message}
                helperText={errors.message ? errors.message : ''}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{ maxLength: 60 }}
                placeholder="El mensaje debe tener al menos 60 caracteres"
              />
            </Container>
          </Grid>
        </Grid>
        <Box
          sx={{
            marginTop: 2,
            textAlign: 'center',
          }}
        >
          <Button type="submit" disabled={processing} variant="contained" color="primary">
            {processing ? <CircularProgress size={20} /> : 'Enviar'}
          </Button>
        </Box>
      </Paper>
    </div>
  );
};

Index.layout = (page: any) => <Layout children={page} title="Notificaciones" />;

export default Index;
