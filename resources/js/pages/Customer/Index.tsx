import { Inertia, Page } from '@inertiajs/inertia';
import { usePage } from '@inertiajs/inertia-react';
import React from 'react';
import { CellProps, Column } from 'react-table';
import { Customer, DefaultPageProps } from 'resources/js/types';
import Layout from '../../layouts/Layout';
import {
    Edit as EditIcon,
    Visibility as VisibilityIcon,
    Add as AddIcon,
    ToggleOff as MuiToggleOff,
    ToggleOn as MuiToggleOn,
    Refresh as RefreshIcon,
} from '@mui/icons-material';
import { TableContainer, Paper, makeStyles, IconButton, Tooltip, Button } from '@mui/material';
import SelectColumnFilter from '@common/components/data-table/filters/SelectColumnFilter';
// @ts-ignore
import route from 'ziggy';
import DataTable from '@common/components/data-table';
import { styled } from '@mui/material/styles';

const ACTIVE_FILTER_OPTIONS = [
    { label: 'Todos', value: '' },
    { label: 'Activo', value: '1' },
    { label: 'Inactivo', value: '0' },
];

const RESOURCE = 'customers';

const ToggleOn = styled(MuiToggleOn)(({ theme }) => ({
    color: 'green',
    fontSize: '2.5rem',
}));

const ToggleOff = styled(MuiToggleOn)(({ theme }) => ({
    fontSize: '2.5rem',
}));

type PageProps = {
    data: {
        data: Array<Customer>;
        current_page: number;
        total: number;
        per_page: number;
    };
};

// toolbarAction: {
//     marginRight: theme.spacing(1),
// },

const Index = ({}) => {
    const { data: pagination } = usePage<Page<PageProps & DefaultPageProps>>().props;

    const data = React.useMemo(() => pagination.data, [pagination.data]);

    const columns: Array<Column<Customer>> = React.useMemo(
        () => [
            {
                Header: 'ID',
                accessor: 'id',
                width: 100,
                // defaultCanSort: true,
            },
            {
                Header: 'Nombre',
                accessor: 'name',
                // defaultCanSort: true,
            },
            {
                Header: 'Clave SAP',
                accessor: 'sapKey',
                // defaultCanSort: true,
            },
            {
                Header: 'Activo',
                accessor: 'active',
                Cell: ({ value }: { value: boolean }) => {
                    if (value) {
                        return <ToggleOn />;
                    }

                    return (
                        <ToggleOff
                            sx={{
                                fontSize: '2.5rem',
                            }}
                        />
                    );
                },
                disableSortBy: true,
                Filter: (props) => <SelectColumnFilter column={props.column} options={ACTIVE_FILTER_OPTIONS} />,
            },
        ],
        []
    );

    const onChangeTable = ({
        page,
        perPage,
        // @ts-ignore
        sort,
        filters,
    }: {
        page: number;
        perPage: number;
        sort?: string;
        filters?: { [key: string]: any };
    }) => {
        Inertia.get(
            route(`${RESOURCE}.index`),
            {
                page,
                per_page: perPage,
                // @ts-ignore
                sort,
                // @ts-ignore
                filter: filters,
            },
            {
                preserveState: true,
                preserveScroll: true,
                only: ['data'],
            }
        );
    };

    const additionalColumns: Array<Column<Customer>> = React.useMemo(
        () => [
            {
                id: 'actions',
                Header: 'Acciones',
                width: 100,
                Cell: ({ row }: React.PropsWithChildren<CellProps<Customer>>) => (
                    <>
                        <Tooltip title="Ver detalles">
                            <IconButton
                                sx={{
                                    padding: 0.5,
                                }}
                                onClick={() => {
                                    Inertia.visit(route(`${RESOURCE}.show`, [row.original.id]));
                                }}
                            >
                                <VisibilityIcon
                                    sx={{
                                        fontSize: '1.3rem',
                                    }}
                                />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Editar">
                            <IconButton
                                sx={{
                                    padding: 0.5,
                                }}
                                onClick={() => {
                                    Inertia.visit(route(`${RESOURCE}.edit`, [row.original.id]));
                                }}
                            >
                                <EditIcon
                                    sx={{
                                        fontSize: '1.3rem',
                                    }}
                                />
                            </IconButton>
                        </Tooltip>
                    </>
                ),
            },
        ],
        []
    );

    return (
        <>
            <TableContainer component={Paper}>
                <DataTable
                    columns={columns}
                    data={data}
                    currentPage={pagination.current_page}
                    currentPerPage={pagination.per_page}
                    totalCount={pagination.total}
                    onChange={onChangeTable}
                    additionalColumns={additionalColumns}
                    renderToolbar={
                        <>
                            <Button
                                onClick={() => {
                                    Inertia.visit(route(`${RESOURCE}.create`));
                                }}
                                sx={{
                                    marginRight: 1,
                                }}
                                variant="outlined"
                                color="primary"
                                size="small"
                                startIcon={<AddIcon />}
                            >
                                Nuevo
                            </Button>
                            <Tooltip title="Recargar resultados">
                                <IconButton
                                    sx={{
                                        padding: 0.5,
                                    }}
                                    onClick={() => {
                                        Inertia.reload();
                                    }}
                                >
                                    <RefreshIcon />
                                </IconButton>
                            </Tooltip>
                        </>
                    }
                />
            </TableContainer>
        </>
    );
};

Index.layout = (page: any) => <Layout children={page} title="Clientes" />;

export default Index;
