import React from 'react';
import Layout from '../../layouts/Layout';
import {
    Button,
    Container,
    Paper,
    TextField,
    FormControl,
    FormControlLabel,
    FormGroup,
    FormLabel,
    Switch,
    CircularProgress,
    Box,
} from '@mui/material';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Customer, DefaultPageProps } from '../../types';
import { useSnackbar } from 'notistack';
// @ts-ignore
import route from 'ziggy';
import { LoadingButton } from '@mui/lab';
import { Page } from '@inertiajs/inertia';

const RESOURCE = 'customers.update';

type PageProps = {
    data: Customer;
}

const Edit = () => {
    const { enqueueSnackbar } = useSnackbar();

    const { data: saved } = usePage<Page<PageProps & DefaultPageProps>>().props;

    const { data, setData, patch, processing, errors, reset } = useForm<Omit<Customer, 'id'>>(saved);

    const submit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        patch(route(RESOURCE), {
            onSuccess: (props) => {
                console.log('the props', props);
                reset();
                enqueueSnackbar('Cliente creado exitósamente.', {
                    variant: 'success',
                });
            },
            onError: () => {
                enqueueSnackbar('Se produjo un error.', {
                    variant: 'error',
                });
            },
        });
    };

    return (
        <div>
            <Paper
                sx={{
                    padding: 2,
                }}
            >
                <Container maxWidth="sm">
                    <form onSubmit={submit}>
                        <TextField
                            label="Nombre del cliente"
                            value={data.name}
                            onChange={(e) => setData('name', e.target.value)}
                            fullWidth
                            variant="outlined"
                            error={!!errors.name}
                            helperText={errors.name ? errors.name : ''}
                            required
                            margin="dense"
                        />
                        <TextField
                            label="Clave SAP"
                            value={data.sapKey}
                            onChange={(e) => setData('sapKey', e.target.value)}
                            fullWidth
                            variant="outlined"
                            error={!!errors.sapKey}
                            helperText={errors.sapKey ? errors.sapKey : ''}
                            // required
                            margin="dense"
                        />
                        <FormControl component="fieldset" sx={{ margin: 10 }}>
                            <FormLabel component="legend">Activo</FormLabel>
                            <FormGroup>
                                <FormControlLabel
                                    control={
                                        <Switch
                                            color="primary"
                                            onChange={() => {
                                                setData('active', !data.active);
                                            }}
                                            checked={data.active}
                                        />
                                    }
                                    label={data.active ? 'Si' : 'No'}
                                />
                            </FormGroup>
                        </FormControl>

                        <Box
                            sx={{
                                marginTop: 2,
                                textAlign: 'center',
                            }}
                        >
                            <LoadingButton type="submit" disabled={processing} variant="contained" color="primary">
                                Registrar
                            </LoadingButton>
                        </Box>
                    </form>
                </Container>
            </Paper>
        </div>
    );
};

Edit.layout = (page: any) => <Layout children={page} title="Crear cliente" />;

export default Edit;
