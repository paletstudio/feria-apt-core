import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
} from '@mui/material';
import { DefaultPageProps, Version } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
import { useSnackbar } from 'notistack';
// @ts-ignore
import route from 'ziggy';
import NumberFormat from '@common/components/NumberFormat';

type PageProps = {
  data: Version;
};

const RESOURCE = 'version.store';

const Index = () => {
  const { data: versionProp } = usePage<Page<PageProps & DefaultPageProps>>().props;
  const { data, setData, post, processing, errors, reset } = useForm<Version>({
    version: versionProp ? versionProp.version : '',
  });
  const { enqueueSnackbar } = useSnackbar();

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    post(route(RESOURCE), {
      onSuccess: () => {
        enqueueSnackbar('Versión actualizada exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <Paper
        onSubmit={submit}
        component="form"
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <TextField
            label="Versión"
            value={data.version}
            fullWidth
            variant="outlined"
            margin="dense"
            onChange={(e) => setData('version', e.target.value)}
            error={!!errors.version}
            helperText={errors.version ? errors.version : ''}
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              inputComponent: NumberFormat as any,
            }}
          />
          <Box
            sx={{
              marginTop: 2,
              textAlign: 'center',
            }}
          >
            <Button type="submit" disabled={processing} variant="contained" color="primary">
              {processing ? <CircularProgress size={20} /> : 'Actualizar'}
            </Button>
          </Box>
        </Container>
      </Paper>
    </div>
  );
};

Index.layout = (page: any) => <Layout children={page} title="Versión" />;

export default Index;
