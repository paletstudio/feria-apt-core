import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
} from '@mui/material';
import { Advice, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
import { CKEditor } from '@ckeditor/ckeditor5-react';
// @ts-ignore
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Dropzone from '../../common/components/DropzoneImage';

type PageProps = {
  data: Advice;
};

const Show = () => {
  const { data } = usePage<Page<PageProps & DefaultPageProps>>().props;

  return (
    <div>
      <Paper
        sx={{
          padding: 2,
        }}
      >
        <Container maxWidth="sm">
          <TextField label="Título" value={data.titulo} fullWidth variant="outlined" margin="dense" />
          <TextField label="Subtítulo" value={data.subtitulo} fullWidth variant="outlined" margin="dense" />
          <TextField label="Fecha" value={data.fecha} fullWidth variant="outlined" margin="dense" />
          <FormControl fullWidth variant="outlined" margin="dense">
            <InputLabel
              shrink
              style={{
                marginBottom: 100,
              }}
            >
              Descripción
            </InputLabel>
            <Box
              sx={{
                marginTop: 1,
              }}
            >
              <CKEditor
                id="descriptionArea"
                config={{
                  language: 'es',
                  addMainLanguageTranslationsToAllAssets: true,
                }}
                disabled={true}
                editor={ClassicEditor}
                data={data.descripcion}
                onReady={(editor: any) => {
                  // You can store the "editor" and use when it is needed.
                  console.log('Editor is ready to use!', editor);
                }}
                onChange={(event: any, editor: any) => {
                  const data = editor.getData();
                  console.log({ event, editor, data });
                }}
                onBlur={(event: any, editor: any) => {
                  console.log('Blur.', editor);
                }}
                onFocus={(event: any, editor: any) => {
                  console.log('Focus.', editor);
                }}
              />
            </Box>
          </FormControl>
          <Dropzone
            disabled
            getCurrentImage={(image: any) => {
              // setData('imagen', image[0]);
            }}
            file={data.imagen ? data.imagen : null}
            title="seleciona la imagen del aviso"
          />
          <FormControl component="fieldset" style={{ margin: 10 }}>
            <FormLabel component="legend">Activo</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={<Switch color="primary" checked={data.activo} />}
                label={data.activo ? 'Si' : 'No'}
              />
            </FormGroup>
          </FormControl>
        </Container>
      </Paper>
    </div>
  );
};

Show.layout = (page: any) => <Layout children={page} title="Detalles de aviso" />;

export default Show;
