import React, { useEffect } from 'react';
import Layout from '../../layouts/Layout';
import {
  Button,
  Container,
  makeStyles,
  Paper,
  TextField,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Switch,
  CircularProgress,
  Grid,
  Box,
  InputLabel,
  InputAdornment,
  FormHelperText,
} from '@mui/material';
import { Advice, DefaultPageProps } from '../../types';
import { useForm, usePage } from '@inertiajs/inertia-react';
import { Page } from '@inertiajs/inertia';
import { CKEditor } from '@ckeditor/ckeditor5-react';
// @ts-ignore
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// @ts-ignore
import route from 'ziggy';
import { useSnackbar } from 'notistack';
import MobileDatePicker from '@mui/lab/MobileDatePicker';
import { CalendarToday as CalendarIcon } from '@mui/icons-material';
import Dropzone from '../../common/components/DropzoneImage';

type PageProps = {
  data: Advice;
};

const RESOURCE = 'advices.update';

const Edit = () => {
  const { data: adviceProp } = usePage<Page<PageProps & DefaultPageProps>>().props;
  const { data, setData, post, processing, errors, reset, transform } = useForm<Advice & { _method: string }>({
    titulo: adviceProp.titulo,
    subtitulo: adviceProp.subtitulo,
    fecha: adviceProp.fecha,
    descripcion: adviceProp.descripcion,
    activo: adviceProp.activo,
    id: adviceProp.id,
    imagen: adviceProp.imagen,
    _method: 'PUT',
  });
  const { enqueueSnackbar } = useSnackbar();

  const submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    transform((prev: any) => {
      return {
        ...prev,
        _method: 'PUT',
      };
    });
    post(route(RESOURCE, [data.id]), {
      onSuccess: () => {
        enqueueSnackbar('Aviso actualizado exitósamente.', {
          variant: 'success',
        });
      },
      onError: () => {
        enqueueSnackbar('Se produjo un error.', {
          variant: 'error',
        });
      },
    });
  };

  return (
    <div>
      <form onSubmit={submit}>
        <Paper
          sx={{
            padding: 2,
          }}
        >
          <Container maxWidth="sm">
            <TextField
              label="Título"
              value={data.titulo}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('titulo', e.target.value)}
              error={!!errors.titulo}
              helperText={errors.titulo ? errors.titulo : ''}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              label="Subtítulo"
              value={data.subtitulo}
              fullWidth
              variant="outlined"
              margin="dense"
              onChange={(e) => setData('subtitulo', e.target.value)}
              error={!!errors.subtitulo}
              helperText={errors.subtitulo ? errors.subtitulo : ''}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <MobileDatePicker
              label="Fecha"
              clearText="Limpiar"
              cancelText="Cancelar"
              clearable
              value={data.fecha}
              onChange={(date: any) => {
                setData('fecha', date ? date.format('YYYY-MM-DD') : null);
              }}
              inputFormat="YYYY-MM-DD"
              toolbarTitle="Elige una fecha"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <CalendarIcon style={{ fontSize: 20 }} />
                  </InputAdornment>
                ),
              }}
              renderInput={(params) => (
                <TextField
                  InputLabelProps={{
                    shrink: true,
                  }}
                  fullWidth
                  sx={{
                    '& p': {
                      color: '#D32F2F',
                    },
                  }}
                  error={!!errors.fecha}
                  helperText={errors.fecha ? errors.fecha : ''}
                  {...params}
                />
              )}
            />
            <FormControl fullWidth variant="outlined" margin="dense">
              <InputLabel
                shrink
                style={{
                  marginBottom: 100,
                }}
              >
                Descripción
              </InputLabel>
              <Box
                sx={{
                  marginTop: 1,
                }}
              >
                <CKEditor
                  id="descriptionArea"
                  config={{
                    language: 'es',
                    addMainLanguageTranslationsToAllAssets: true,
                  }}
                  editor={ClassicEditor}
                  data={data.descripcion}
                  onReady={(editor: any) => {
                    // You can store the "editor" and use when it is needed.
                    console.log('Editor is ready to use!', editor);
                  }}
                  onChange={(event: any, editor: any) => {
                    const data = editor.getData();
                    setData('descripcion', data);
                  }}
                  onBlur={(event: any, editor: any) => {
                    console.log('Blur.', editor);
                  }}
                  onFocus={(event: any, editor: any) => {
                    console.log('Focus.', editor);
                  }}
                />
              </Box>
              <FormHelperText error={!!errors.descripcion}>
                {errors.descripcion ? errors.descripcion : ''}
              </FormHelperText>
            </FormControl>
            <Dropzone
              getCurrentImage={(image: any) => {
                setData((prev: any) => {
                  return {
                    ...prev,
                    imagen: image[0],
                  };
                });
                // setData('imagen', );
              }}
              file={data.imagen ? data.imagen : null}
              title="seleciona la imagen del aviso"
              error={!!errors.imagen}
              helperText={errors.imagen ? errors.imagen : ''}
            />
            <FormControl component="fieldset" style={{ margin: 10 }}>
              <FormLabel component="legend">Activo</FormLabel>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Switch
                      color="primary"
                      checked={data.activo}
                      onChange={() => {
                        setData('activo', !data.activo);
                      }}
                    />
                  }
                  label={data.activo ? 'Si' : 'No'}
                />
              </FormGroup>
            </FormControl>
            <Box
              sx={{
                marginTop: 2,
                textAlign: 'center',
              }}
            >
              <Button type="submit" disabled={processing} variant="contained" color="primary">
                {processing ? <CircularProgress size={20} /> : 'Actualizar'}
              </Button>
            </Box>
          </Container>
        </Paper>
      </form>
    </div>
  );
};

Edit.layout = (page: any) => <Layout children={page} title="Editar aviso" />;

export default Edit;
