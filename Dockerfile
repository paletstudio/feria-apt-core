FROM trafex/php-nginx:2.2.0 as base

ARG GID

USER root

RUN apk --update --no-cache add \
    php8-zip php8-iconv php8-pdo_mysql php8-fileinfo php8-tokenizer php8-xmlwriter php8-mbstring php8-bcmath php8-gd  \
    nodejs npm yarn shadow nano \
    php8-simplexml libpng-dev

RUN groupmod -o -g $GID nobody

RUN chown -R nobody.nobody /var/www/html && \
    chown -R nobody.nobody /run && \
    chown -R nobody.nobody /var/lib/nginx && \
    chown -R nobody.nobody /var/log/nginx && \
    mkdir /.config && chown -R nobody.nobody /.config && \
    mkdir /.composer && chown -R nobody.nobody /.composer && \
    mkdir /.cache && chown -R nobody.nobody /.cache && \
    mkdir /.yarn && chown -R nobody.nobody /.yarn

COPY docker/production/config/php.ini /etc/php8/conf.d/custom.ini
COPY docker/production/config/nginx.conf /etc/nginx/nginx.conf
COPY docker/production/start-container.sh /usr/local/bin/start-container.sh

RUN chmod +x /usr/local/bin/start-container.sh

USER nobody


FROM base as dev

COPY --from=composer /usr/bin/composer /usr/bin/composer

CMD [ "/usr/local/bin/start-container.sh" ]


FROM dev as builder

COPY database /var/www/html/database
COPY composer.lock composer.json /var/www/html/

RUN composer install \
    --optimize-autoloader \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --no-dev


FROM base as prod

WORKDIR /var/www/html/

COPY --from=builder --chown=nobody /var/www/html/vendor/ ./vendor/

COPY --chown=nobody . .

CMD [ "/usr/local/bin/start-container.sh" ]
