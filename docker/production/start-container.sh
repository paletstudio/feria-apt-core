#!/bin/sh

set -e

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-local}

if [ "$env" != "local" ]; then
    echo "--- Caching configuration..."
    (cd /var/www/html && php artisan optimize)
fi

if [ "$role" = "app" ]; then

    cd /var/www/html

    echo "--- php artisan commands"
    php artisan storage:link && php artisan migrate --force
    # && php artisan optimize

    echo "--- yarn"
    yarn && yarn run prod

    exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf

elif [ "$role" = "queue" ]; then

    echo "Running the queue..."
    php /var/www/html/artisan queue:work --verbose --tries=3 --timeout=90

elif [ "$role" = "scheduler" ]; then

    while [ true ]; do
        php /var/www/html/artisan schedule:run --verbose --no-interaction &
        sleep 60
    done

else
    echo "Could not match the container role \"$role\""
    exit 1
fi
